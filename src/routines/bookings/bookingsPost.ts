import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { action as bookingsListRoutineActions } from './bookingsList'
import { BookingsActionsTypes } from './../../models/ActionTypes'
import { APIBooking } from './../../models/APIModels'
import { axiosInstance } from './../../utils/requests'

export interface BookingDto {
  timeSlot: {
    to: number
    from: number
  }
  bookingDate: string
  eventStatus?: string
  ownerId: string
  toolId: string
  buddyEmail?: string
}

export const action: Routine = createRoutine<BookingDto, BookingDto>(
  BookingsActionsTypes.POST_BOOKING,
  _.identity
)

async function postBooking(data: BookingDto): Promise<APIBooking> {
  const { bookingDate, timeSlot, ownerId, toolId, eventStatus } = data
  return axiosInstance
    .post(`/bookings`, {
      bookingDate,
      timeSlot,
      ownerId,
      toolId,
      eventStatus: eventStatus ? eventStatus : ''
    })
    .then((resp: AxiosResponse<any>) => resp.data)
    .catch((err: AxiosError) => err.response!.data)
}

function* handler({ payload }: Action<BookingDto>) {
  try {
    yield put(action.request())
    const response = yield call(postBooking, payload)
    if (response.errors) {
      throw Error(response)
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
    yield put(bookingsListRoutineActions())
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
