import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { SpaceFormDto, SpacesActionsTypes, APISpace } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface Payload {
  values: SpaceFormDto
}

export const action: Routine = createRoutine(SpacesActionsTypes.POST_SPACE, _.identity)

async function postSpace(data: SpaceFormDto): Promise<APISpace> {
  console.log(data)
  return axiosInstance
    .post(`/spaces`, {
      title: data.title,
      hudId: data.hubs.value,
      description: data.description,
      workingHours: [data.minWorkTime, data.maxWorkTime]
    })
    .then((resp: AxiosResponse<APISpace>) => resp.data)
    .catch((err: AxiosError) => err.response!.data)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const response = yield call(postSpace, payload!.values)
    if (response.statusCode) {
      yield put(action.failure(response))
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
