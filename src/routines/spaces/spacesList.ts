import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { APITool, SpacesActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface Payload {}

export const action: Routine = createRoutine(SpacesActionsTypes.GET_ALL_SPACES, _.identity)

async function fetchList(): Promise<APITool> {
  return axiosInstance
    .get(`/spaces`)
    .then((resp: AxiosResponse<APITool>) => resp.data)
    .catch((err: AxiosError) => err.response!.data)
}

function* handler() {
  try {
    yield put(action.request())
    const response = yield call(fetchList)
    if (response.statusCode) {
      yield put(action.failure(response))
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
