import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, all, takeEvery } from 'redux-saga/effects'

import { UsersActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

interface Email {
  email: string
  organizationId: string
}
export interface Payload {
  file?: FormData
  email?: Email
}

export const action: Routine = createRoutine<Payload>(UsersActionsTypes.UPLOAD_CSV_FILE)

function addNewUsersCSV(file: FormData): Promise<any> {
  return axiosInstance.post('/auth/sign-up/csv', file)
}

function addNewUsersEmail(payload: Email): Promise<any> {
  return axiosInstance.post('/users', payload)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const [fileResponse, userResponse] = yield all([
      payload.file ? call(addNewUsersCSV, payload.file) : null,
      payload.email ? call(addNewUsersEmail, payload.email) : null
    ])
    console.log(fileResponse, userResponse)
    // TODO: we need to write errors handler

    yield put(action.success({ status }))
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
