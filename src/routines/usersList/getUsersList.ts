import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { replace } from 'connected-react-router'

import { Routes } from '../../utils/routes'
import { APIUserFull, UsersActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface Payload {
  page: number
  search?: string
  sort?: Array<string>
}

interface State {
  router: {
    location: {
      pathname: string
    }
  }
}

export const action: Routine = createRoutine(UsersActionsTypes.GET_ALL_USERS)

function handleGetUsersList(params: Payload): Promise<any> {
  return axiosInstance.get(`/users`, { params })
}

function* handler({ payload }: Action<Payload>) {
  const page = payload ? payload.page : 1
  try {
    yield put(action.request())
    const [usersList, totalUsers] = (yield call(handleGetUsersList, payload)).data as [
      APIUserFull[],
      number
    ]
    if (!usersList.length && page !== 1) {
      yield put(action({ page: 1 }))
      yield put(replace(`${Routes.UsersPage}/1`))
    } else {
      const currentUrl = yield select((state: State) => state.router.location.pathname)
      if (currentUrl.match(/\d+/)[0] !== String(page)) {
        yield put(replace(`${Routes.UsersPage}/${page}`))
      }
      yield put(action.success({ usersList, totalUsers }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
