import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { UsersActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface Payload {
  id: string
  data: {
    role: string
    organizationId: string
  }
}

export const action: Routine = createRoutine(UsersActionsTypes.PROMOTE_USER)

function handlePromoteUser({ id, data }: Payload): Promise<any> {
  return axiosInstance.post(`/users/${id}/promote`, data)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    yield call(handlePromoteUser, payload)
    // TODO: process response
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
