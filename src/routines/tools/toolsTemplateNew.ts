import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { ToolsActionsTypes, APITemplate } from '../../models'
import { axiosInstance } from '../../utils/requests'
import { Action } from 'redux-actions'

export interface Payload {
  values: APITemplate
}

interface ToolArrType {
  value: string
  label: string
}

export const action: Routine = createRoutine(ToolsActionsTypes.CREATE_NEW_TOOL, _.identity)

async function createToolTemplate(data: any): Promise<APITemplate> {
  const toolIds = data.tools ? (data.tools as ToolArrType[]).map(el => el.value) : []
  return axiosInstance
    .post(`/tools/template`, {
      availableInWorkingHours: data.availableInWorkingHours,
      buddyMode: data.buddyMode,
      description: data.description,
      maxWorkTime: data.maxWorkTime,
      minWorkTime: data.minWorkTime,
      requiresTraining: data.requiresTraining,
      sound: data.sound,
      title: data.title,
      organizationId: data.organizationId,
      tools: toolIds
    })
    .then((resp: AxiosResponse<APITemplate>) => resp.data)
    .catch((err: AxiosError) => err.response!.data)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const response = yield call(createToolTemplate, payload!.values)
    if (response.statusCode) {
      yield put(action.failure(response))
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
