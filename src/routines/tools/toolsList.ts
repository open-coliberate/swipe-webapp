import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { APITool, ToolsActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface Payload {}

export const action: Routine = createRoutine(ToolsActionsTypes.GET_ALL_TOOLS, _.identity)

async function fetchList(): Promise<APITool> {
  return axiosInstance
    .get(`/tools?page=1`)
    .then((resp: AxiosResponse<APITool>) => resp.data)
    .catch((err: AxiosError) => err.response!.data)
}

function* handler() {
  try {
    yield put(action.request())
    const response = yield call(fetchList)
    if (response.statusCode) {
      yield put(action.failure(response))
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
