import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { APITemplate, APIError, ToolsActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

type ResponseType = APIError | { data: APITemplate[] }
export interface Payload {
  page: number
}

export const action: Routine = createRoutine<Payload>(ToolsActionsTypes.GET_ALL_TEMPLATES)

async function fetchList(params: Payload): Promise<any> {
  return axiosInstance.get(`/tools/templates`, { params })
}

function* handler() {
  // TODO: Need to add ability transpand page nuber from component
  const params = { page: 1 }

  try {
    yield put(action.request())
    const response: ResponseType = yield call(fetchList, params)

    if ('errors' in response) {
      throw Error(response.message)
    } else {
      yield put(action.success({ response: response.data }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
