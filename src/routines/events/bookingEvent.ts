import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { EventsActionsTypes } from './../../models/ActionTypes'
import { APIEventBooking, APIError } from './../../models/APIModels'
import { axiosInstance } from './../../utils/requests'

type ResponseType = APIError | { data: APIEventBooking[] }

export const action: Routine = createRoutine<APIEventBooking>(EventsActionsTypes.POST_EVENT)

function postBooking(data: APIEventBooking): Promise<any> {
  return axiosInstance.post('/events', data)
}

function* handler({ payload }: Action<APIEventBooking>) {
  try {
    yield put(action.request())
    const response: ResponseType = yield call(postBooking, payload)
    if ('errors' in response) {
      throw Error(response.message)
    } else {
      yield put(action.success({ response: response.data[0] }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
