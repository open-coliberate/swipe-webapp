import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { EventsActionsTypes } from './../../models/ActionTypes'
import { APIEventBooking, APIError } from './../../models/APIModels'
import { action as getBookingEventList } from './bookingEvent'
import { axiosInstance } from './../../utils/requests'

type ResponseType = APIError | { data: APIEventBooking[] }

export const action: Routine = createRoutine(EventsActionsTypes.GET_ALL_EVENTS)

function getBooking(): Promise<any> {
  return axiosInstance.get('/events')
}

function* handler() {
  try {
    yield put(action.request())
    const response: ResponseType = yield call(getBooking)
    if ('errors' in response) {
      throw Error(response.message)
    } else {
      yield put(action.success({ response: response.data }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
    yield put(getBookingEventList())
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
