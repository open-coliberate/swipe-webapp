import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { handleGetUserTemplatesPermission } from '../user/getUserTemplatesPermission'
import { UsersActionsTypes, Roles } from '../../models'
import { axiosInstance } from '../../utils/requests'

interface Payload {
  id: string
}

export const action: Routine = createRoutine(UsersActionsTypes.GET_USER_PROFILE)

function handleRequest(id: Payload): Promise<any> {
  return axiosInstance.get(`/users/${id}`)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const profile = yield call(handleRequest, payload)
    if (profile.errors) throw Error(profile)
    const user = { ...profile.data }
    // Get tools permission if user role is 'GENERAL_USER' otherwise null
    const toolsPermission =
      user.role === Roles.GENERAL_USER
        ? yield call(handleGetUserTemplatesPermission, user.id)
        : null
    user.toolsPermission =
      toolsPermission && !('errors' in toolsPermission) ? toolsPermission.data : null

    yield put(action.success(user))
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
