import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { HubsActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

interface Payload {
  id: string
}

export const action: Routine = createRoutine<Payload>(HubsActionsTypes.DELETE_HUB)

function deleteHub(id: Payload): Promise<any> {
  return axiosInstance.delete(`/hubs/${id}`)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    yield call(deleteHub, payload)
    yield put(action.success({ id: payload }))
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
