import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery, select } from 'redux-saga/effects'

import { APIHubCreateResponse, HubsActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'
import { UserFullExtended } from '../../models'

interface Payload {
  title: string
  address: string
  hubID?: string
}

interface PayloadExtended extends Payload {
  organizationId: string
}

export const action: Routine = createRoutine<Payload>(HubsActionsTypes.CREATE_HUB)

function createHub(payload: PayloadExtended): Promise<any> {
  return axiosInstance.post('/hubs', payload)
}
function editHub(payload: PayloadExtended): Promise<any> {
  const id = payload.hubID || ''
  delete payload.hubID
  return axiosInstance.patch(`/hubs/${id}`, payload)
}

function* handler({ payload }: Action<Payload>) {
  const organizationId = yield select(
    ({ user }: { user: UserFullExtended }) => user.organization.id
  )
  const newPayload = {
    ...payload,
    organizationId
  }

  try {
    yield put(action.request())
    const response = payload.hubID
      ? ((yield call(editHub, newPayload)).data as APIHubCreateResponse)
      : ((yield call(createHub, newPayload)).data as APIHubCreateResponse)
    yield put(action.success({ response }))
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
