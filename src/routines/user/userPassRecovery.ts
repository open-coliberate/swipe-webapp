import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { Action } from 'redux-actions'
import { createRoutine, Routine, ReduxFormPayload } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { UserActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface FormValues {
  email: string
}

export type Payload = ReduxFormPayload<FormValues>

export const action: Routine = createRoutine<Payload, Payload>(
  UserActionsTypes.RESET_PASSWORD,
  _.identity
)

async function handleRecover(email: string): Promise<any> {
  return axiosInstance
    .put(`/auth/reset-password-request`, { email })
    .then((resp: AxiosResponse<any>) => {
      console.log(resp)
      return resp.data
    })
    .catch((err: AxiosError) => err.response!.data)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const resp = yield call(handleRecover, payload!.values.email)
    yield put(action.success({ resp }))
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
