import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { Action } from 'redux-actions'
import { createRoutine, Routine, ReduxFormPayload } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { APIUserFull, UserActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface FormValues {
  email: string
  password: string
}

export type Payload = ReduxFormPayload<FormValues>

export const action: Routine = createRoutine<Payload, Payload>(UserActionsTypes.LOGIN, _.identity)

async function handleLogin(vals: FormValues): Promise<any> {
  const { email, password } = vals
  return axiosInstance
    .post(`/auth/sign-in`, { email, password })
    .then((resp: AxiosResponse<APIUserFull>) => resp.data)
    .catch((err: AxiosError) => err.response!.data)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const response = yield call(handleLogin, payload!.values)
    if (response.statusCode) {
      yield put(action.failure(response))
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
