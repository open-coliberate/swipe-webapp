import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { APIUserToolsPermission, APIError, UsersActionsTypes } from '../../models'
import { axiosInstance } from '../../utils/requests'

type Payload = {
  id: string
}
type ResponseType = APIError | { data: APIUserToolsPermission }

export const action: Routine = createRoutine<Payload>(
  UsersActionsTypes.GET_ALLOWED_TEMPLATES_FOR_USER
)

export function handleGetUserTemplatesPermission(id: string): Promise<any> {
  return axiosInstance.get(`/users/${id}/trainings`)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const response: ResponseType = yield call(handleGetUserTemplatesPermission, payload.id)

    if ('errors' in response) {
      throw Error(response.message)
    } else {
      yield put(action.success(response))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
