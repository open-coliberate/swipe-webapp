// import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { action as changeAllowableTools } from './changeAllowableTools'
import { action as setWorkingHours } from './setUserWorkingHours'
import { action as changePassword } from './userChangePass'
import { UserActionsTypes } from '../../models'
import { axiosInstance } from '../../utils/requests'
import { UserInfo } from './../../components/ProfileForm'

export interface Payload extends UserInfo {
  oldPassword: string
}

type UserValues = {
  id: string
  email: string
  firstName: string
  lastName: string
  phone?: string
  description?: string
}

export const action: Routine = createRoutine<UserInfo>(UserActionsTypes.UPDATE_MY_INFO)

function updateInfo({ id, ...payload }: UserValues): Promise<any> {
  delete payload.email // TODO: Probably remove this line in feature
  return axiosInstance.patch(`/users/${id}`, payload)
}

function* handler({ payload }: Action<Payload>) {
  const {
    workingHours,
    password,
    passwordConfirmation,
    oldPassword,
    toolsAllowed,
    ...userData
  } = payload

  const passPayload =
    password && passwordConfirmation
      ? {
          password,
          passwordConfirmation,
          oldPassword
        }
      : null

  try {
    yield put(action.request())
    const response = yield call(updateInfo, userData)
    if (passPayload) {
      yield put(changePassword(passPayload))
    }
    if (toolsAllowed) {
      const tools = toolsAllowed.map(({ id }) => id)
      yield put(changeAllowableTools({ id: payload.id, tools }))
    }
    if (workingHours.length) {
      yield put(setWorkingHours({ id: payload.id, items: workingHours }))
    }
    if (response.data) {
      yield put(action.success({ response }))
    } else {
      yield put(action.failure(response))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
