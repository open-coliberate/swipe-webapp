import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { UsersActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface Payload {
  tools: string[]
  id: string
}

export const action: Routine = createRoutine<Payload>(UsersActionsTypes.ALLOW_TEMPLATE_FOR_USER)

function requestHandler({ id, tools: toolTemplatesIds }: Payload): Promise<any> {
  return axiosInstance.post(`/users/${id}/allow-tool-template`, { toolTemplatesIds })
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const response = yield call(requestHandler, payload)
    if (response.errors) {
      yield put(action.failure(response))
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
