import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { APIUserFull, UserActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface Payload {
  id: string
}

export const action: Routine = createRoutine<Payload, Payload>(
  UserActionsTypes.GET_USER,
  _.identity
)

async function handleMyInfo(id: string): Promise<APIUserFull> {
  return axiosInstance
    .get(`/users/${id}`)
    .then((resp: AxiosResponse<APIUserFull>) => resp.data)
    .catch((err: AxiosError) => err.response!.data)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const response = yield call(handleMyInfo, payload!.id)
    if (response.errors) {
      yield put(action.failure(response))
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
