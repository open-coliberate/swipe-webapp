import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { APIWorkingHoursType, APIError, UsersActionsTypes } from '../../models'
import { axiosInstance } from '../../utils/requests'

type Payload = {
  id: string
  items: APIWorkingHoursType[]
}

type ResponseType = APIError | { data: APIWorkingHoursType }

export const action: Routine = createRoutine<Payload>(UsersActionsTypes.SET_WORKING_HOURS)

export function handleSetWorkingTime({ items, id }: Payload): Promise<any> {
  return axiosInstance.put(`/users/${id}/working-hours`, { items })
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const response: ResponseType = yield call(handleSetWorkingTime, payload)

    if ('errors' in response) {
      throw Error(response.message)
    } else {
      yield put(action.success(response))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
