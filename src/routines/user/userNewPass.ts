import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { Action } from 'redux-actions'
import { createRoutine, Routine, ReduxFormPayload } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { UserActionsTypes } from '../../models'
import { SetPassProps } from '../../pages/SetPass'
import { axiosInstance } from './../../utils/requests'

export interface FormValues {
  password: string
  passwordConfirmation: string
}

export type Payload = ReduxFormPayload<FormValues, SetPassProps>

export const action: Routine = createRoutine<Payload, Payload>(
  UserActionsTypes.SET_PASSWORD,
  _.identity
)

async function handleReset(
  token: string,
  password: string,
  passwordConfirmation: string
): Promise<any> {
  return axiosInstance
    .put(`/auth/reset-password`, { token, password, passwordConfirmation })
    .then((resp: AxiosResponse<any>) => {
      console.log(resp)
      return resp.data
    })
    .catch((err: AxiosError) => err.response!.data)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const userToken = payload!.props.match.params.id
    const resp = yield call(
      handleReset,
      userToken,
      payload!.values.password,
      payload!.values.passwordConfirmation
    )
    yield put(action.success({ resp }))
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
