import { AxiosError, AxiosResponse } from 'axios'
import * as _ from 'lodash'
import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery } from 'redux-saga/effects'

import { UserActionsTypes } from '../../models'
import { APIUserFull } from './../../models/APIModels'
import { axiosInstance } from './../../utils/requests'

export interface Payload {
  refreshToken: string
}

export const action: Routine = createRoutine<Payload, Payload>(
  UserActionsTypes.REFRESH_ACCESS_TOKEN,
  _.identity
)

async function handleRefresh(refreshToken: string): Promise<APIUserFull> {
  console.log(refreshToken)
  return axiosInstance
    .post(`/auth/refresh-token`, { refreshToken })
    .then((resp: AxiosResponse<APIUserFull>) => resp.data)
    .catch((err: AxiosError) => err.response!.data)
}

function* handler({ payload }: Action<Payload>) {
  try {
    yield put(action.request())
    const response = yield call(handleRefresh, payload!.refreshToken)
    if (response.statusCode) {
      yield put(action.failure(response))
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
