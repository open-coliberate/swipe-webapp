import { Action } from 'redux-actions'
import { createRoutine, Routine } from 'redux-saga-routines'
import { call, put, takeEvery, select } from 'redux-saga/effects'

import { RootState } from '../../reducers'
import { UserActionsTypes } from '../../models'
import { axiosInstance } from './../../utils/requests'

export interface Payload {
  password: string
  passwordConfirmation: string
  token: string
}

export const action: Routine = createRoutine<Payload>(UserActionsTypes.CHANGE_PASSWORD)

function resetMyPass(data: Payload): Promise<any> {
  return axiosInstance.put('/auth/password', data)
}

function* handler({ payload }: Action<Payload>) {
  const token = yield select(({ user }: RootState) => user.token.accessToken)
  try {
    yield put(action.request())
    const response = yield call(resetMyPass, { ...payload, token })
    if (response.errors) {
      yield put(action.failure(response))
    } else {
      yield put(action.success({ response }))
    }
  } catch (error) {
    yield put(action.failure(error.message))
  } finally {
    yield put(action.fulfill())
  }
}

export function* saga() {
  yield takeEvery(action.TRIGGER, handler)
}
