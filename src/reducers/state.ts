import { BookingDtoExtended } from './../pages/Calendars/CalendarScheduler'
import { RouterState } from 'connected-react-router'
import { FormStateMap } from 'redux-form'

import {
  AlertModel,
  BookingsListExtended,
  // HubExtended,
  HubsExtended,
  // SpaceExtended,
  SpacesListExtended,
  ToolsListExtended,
  ToolsTemplatesListExtended,
  UserFullExtended,
  UsersListReducer,
  EventsListReducer
} from '../models'
import { State as ProfileState } from './profile'

export namespace RootState {
  export type AlertState = AlertModel
}

export interface Spaces {
  spacesList: SpacesListExtended
  // space: SpaceExtended;
}

export interface Hubs extends HubsExtended {}

export interface Tools {
  toolsList: ToolsListExtended
  toolsTemplatesList: ToolsTemplatesListExtended
}

export interface Bookings {
  bookingsList: BookingsListExtended
  bookingItem: BookingDtoExtended
  selectedDate: string
}

export interface RootState {
  user: UserFullExtended
  usersList: UsersListReducer
  tools: Tools
  bookings: Bookings
  hubs: Hubs
  spaces: Spaces
  alert: RootState.AlertState
  router: RouterState
  form: FormStateMap
  events: EventsListReducer
  profile: ProfileState
}
