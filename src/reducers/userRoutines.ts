import Cookies from 'js-cookie'
import addDays from 'date-fns/add_days'
import format from 'date-fns/format'
import { Action } from 'redux-actions'

import * as Actions from '../actions'
import { APIUserFull, RoutineModel, UserFullExtended, Roles } from '../models'

const initialState: UserFullExtended = {
  createdAt: '',
  email: '',
  firstName: '',
  id: '',
  lastName: '',
  role: Roles.GENERAL_USER,
  token: {
    refreshToken: '',
    accessToken: '',
    expiresIn: ''
  },
  description: '',
  isActive: false,
  organization: {
    id: '',
    title: ''
  },
  phone: '',
  photo: '',
  updatedAt: '',
  toolsPermission: null,
  isToSetNewPass: false,
  isUserAuth: false,
  isFetching: false
}

const nextDay = (): string => format(addDays(new Date(), 1))

export default function userRoutines(
  state: UserFullExtended = initialState,
  action: Action<RoutineModel>
): UserFullExtended {
  switch (action.type) {
    case Actions.infoRoutineActions.TRIGGER:
    case Actions.refreshTokenRoutineActions.TRIGGER:
    case Actions.loginRoutineActions.TRIGGER:
      return {
        ...state,
        isFetching: true
      }

    case Actions.loginRoutineActions.SUCCESS: {
      const resp = (action.payload as any).response as APIUserFull
      const expiresIn = nextDay()
      Cookies.set('accessToken', resp.token.accessToken)
      Cookies.set('refreshToken', resp.token.refreshToken)
      Cookies.set('myId', resp.id)
      Cookies.set('expiresIn', expiresIn)
      return {
        ...state,
        createdAt: resp.createdAt,
        email: resp.email,
        firstName: resp.firstName,
        id: resp.id,
        lastName: resp.lastName,
        role: resp.role,
        token: {
          refreshToken: resp.token.refreshToken,
          accessToken: resp.token.accessToken,
          expiresIn
        },
        organization: {
          id: resp.organization.id,
          title: resp.organization.title
        },
        isUserAuth: true
      }
    }

    case Actions.refreshTokenRoutineActions.SUCCESS: {
      const resp = (action.payload as any).response as APIUserFull
      const expiresIn = nextDay()
      Cookies.set('expiresIn', expiresIn)
      Cookies.set('accessToken', resp.token.accessToken)
      Cookies.set('refreshToken', resp.token.refreshToken)
      return {
        ...state,
        createdAt: resp.createdAt,
        email: resp.email,
        firstName: resp.firstName,
        id: resp.id,
        lastName: resp.lastName,
        role: resp.role,
        token: {
          refreshToken: resp.token.refreshToken,
          accessToken: resp.token.accessToken,
          expiresIn
        },
        isUserAuth: true
      }
    }

    case Actions.refreshTokenRoutineActions.FAILURE:
    case Actions.infoRoutineActions.FAILURE:
    case Actions.loginRoutineActions.FAILURE: {
      Cookies.remove('myId')
      Cookies.remove('accessToken')
      Cookies.remove('refreshToken')
      Cookies.remove('expiresIn')
      return {
        ...initialState,
        isUserAuth: false,
        isFetching: false
      }
    }

    case Actions.refreshTokenRoutineActions.FULFILL:
    case Actions.infoRoutineActions.FULFILL:
    case Actions.loginRoutineActions.FULFILL:
      return {
        ...state,
        isFetching: false
      }

    case Actions.setActivation.PERFORM: {
      Cookies.remove('myId')
      Cookies.remove('accessToken')
      Cookies.remove('refreshToken')
      Cookies.remove('expiresIn')
      return {
        ...initialState,
        isUserAuth: false,
        isToSetNewPass: true
      }
    }

    case Actions.resetActivation.PERFORM: {
      return {
        ...state,
        isToSetNewPass: false
      }
    }

    case Actions.getCookies.PERFORM: {
      const id = Cookies.get('myId')
      const accessToken = Cookies.get('accessToken')
      const refreshToken = Cookies.get('refreshToken')
      const expiresIn = Cookies.get('expiresIn')
      if (id && accessToken && refreshToken && expiresIn) {
        return {
          ...state,
          id,
          token: {
            accessToken,
            refreshToken,
            expiresIn
          },
          isUserAuth: true
        }
      }
    }

    case Actions.infoRoutineActions.SUCCESS:
      if (action.payload) {
        const data = (action.payload as any).response as APIUserFull
        return {
          ...state,
          ...data,
          isFetching: false,
          isUserAuth: true
        }
      }

    case Actions.recoverPassRoutineActions.TRIGGER:
    case Actions.recoverPassRoutineActions.SUCCESS:
      return {
        ...state,
        isToSetNewPass: true,
        isFetching: true
      }
    case Actions.recoverPassRoutineActions.FAILURE:
    case Actions.refreshTokenRoutineActions.FULFILL:
    case Actions.recoverPassRoutineActions.FULFILL:
      return {
        ...state,
        isFetching: false
      }

    default:
      return state
  }
}
