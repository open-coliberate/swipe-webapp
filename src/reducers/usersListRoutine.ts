import { Action } from 'redux-actions'

import * as actions from '../actions'
import { APIUserProfile, UsersListReducer, RoutineModel } from '../models'

const initialState: UsersListReducer = {
  list: [],
  totalUsers: 0,
  isFetching: false
}

export default function userRoutines(
  state: UsersListReducer = initialState,
  action: Action<RoutineModel>
): UsersListReducer {
  switch (action.type) {
    case actions.getUsersListActions.TRIGGER:
      return {
        ...state,
        isFetching: true
      }

    case actions.getUsersListActions.SUCCESS: {
      const { usersList, totalUsers } = (action.payload as any) as {
        usersList: APIUserProfile[]
        totalUsers: number
      }
      const list = usersList.map(el => ({
        ...el,
        fullName: `${el.firstName} ${el.lastName}`
      }))
      return {
        ...state,
        list,
        totalUsers,
        isFetching: false
      }
    }
    default:
      return state
  }
}
