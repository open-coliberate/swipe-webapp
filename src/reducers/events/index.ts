import { Action } from 'redux-actions'

import * as actions from '../../actions'
import { EventsListReducer, RoutineModel, APIEventBooking } from '../../models'

const initialState: EventsListReducer = {
  list: [],
  isFetching: false
}

export default function eventsRoutines(
  state: EventsListReducer = initialState,
  action: Action<RoutineModel>
): EventsListReducer {
  switch (action.type) {
    case actions.getBookingEventList.TRIGGER:
    case actions.bookingEventActions.TRIGGER:
      return {
        ...state,
        isFetching: true
      }
    case actions.getBookingEventList.SUCCESS: {
      const list = (action.payload as any).response as APIEventBooking[]
      return {
        ...state,
        // TODO: remove after API was fixed CLBRT-127
        list: list.map((el: APIEventBooking) => ({
          ...el,
          spaceID: '3a0cb256-17d5-40c8-9b98-e5d0a9cf39fe'
        })),
        isFetching: false
      }
    }
    case actions.bookingEventActions.SUCCESS: {
      const item = (action.payload as any).response
      // TODO: remove after API was fixed CLBRT-127
      item.spaceID = '3a0cb256-17d5-40c8-9b98-e5d0a9cf39fe'
      return {
        ...state,
        list: state.list.concat(item),
        isFetching: false
      }
    }

    default:
      return state
  }
}
