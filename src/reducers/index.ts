import { connectRouter, RouterState } from 'connected-react-router'
import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { History } from 'history'

import { alertReducer } from './alerts'
import bookingsList from './bookings/bookingsListRoutines'
import bookingsItem from './bookings/bookingItem'
import bookingDate from './bookings/bookingDate'
import { Bookings, RootState, Tools, Spaces } from './state'
import hubsList from './hubs/hubsListRoutines'

import spacesList from './spaces/spacesListRoutines'
import toolsList from './tools/toolsListRoutines'
import toolsTemplatesList from './tools/toolsTemplatesListRoutines'
import userData from './userRoutines'
import usersListData from './usersListRoutine'
import profile from './profile'
import eventsList from './events'

// NOTE: current type definition of Reducer in 'connected-react-router' and 'redux-actions' module
// doesn't go well with redux@4

const spacesReducer = combineReducers<Spaces>({
  spacesList: spacesList as any
  // space: toolsTemplatesList as any
})

const toolsReducer = combineReducers<Tools>({
  toolsList: toolsList as any,
  toolsTemplatesList: toolsTemplatesList as any
})

const bookingsReducer = combineReducers<Bookings>({
  bookingsList: bookingsList as any,
  bookingItem: bookingsItem as any,
  selectedDate: bookingDate
})

export { RootState, RouterState }

export default (history: History) =>
  combineReducers({
    user: userData,
    usersList: usersListData,
    tools: toolsReducer,
    bookings: bookingsReducer,
    spaces: spacesReducer,
    hubs: hubsList,
    form: formReducer,
    alert: alertReducer,
    events: eventsList,
    profile,
    router: connectRouter(history)
  })
