import { Action } from 'redux-actions'

import * as Actions from '../../actions'
import { APIHub, RoutineModel, HubsExtended, ActionWithStringPayload } from '../../models'

const initialState: HubsExtended = {
  currHub: '',
  data: [],
  isFetching: false
}

export default function hubsListRoutines(
  state: HubsExtended = initialState,
  action: Action<RoutineModel> | ActionWithStringPayload
): HubsExtended {
  switch (action.type) {
    case Actions.hubsListRoutineActions.TRIGGER:
      return {
        ...state,
        isFetching: true
      }
    case Actions.hubsListRoutineActions.SUCCESS:
      const data = (action.payload as any).response as APIHub[]
      const currHub = data.length ? data[0].id : ''
      return {
        ...state,
        currHub,
        data
      }
    case Actions.setCurrHub.PERFORM:
      return {
        ...state,
        currHub: action.payload
      }
    case Actions.deleteHub.SUCCESS: {
      const { id } = action.payload as any
      const data = state.data.filter(el => id !== el.id)
      return {
        ...state,
        data
      }
    }
    case Actions.hubsListRoutineActions.FAILURE:
    case Actions.hubsListRoutineActions.FULFILL:
      return {
        ...state,
        isFetching: false
      }
    default:
      return state
  }
}
