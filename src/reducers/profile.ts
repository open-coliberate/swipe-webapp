import { Action } from 'redux-actions'

import * as actions from '../actions'
import { APIUserProfileWitTools, RoutineModel } from '../models'

export type State = {
  info: APIUserProfileWitTools | null
  isFetching: boolean
}

const initialState: State = {
  info: null,
  isFetching: false
}

export default function userRoutines(
  state: State = initialState,
  action: Action<RoutineModel>
): State {
  switch (action.type) {
    case actions.getUserProfile.TRIGGER:
      return {
        ...state,
        isFetching: true
      }
    case actions.getUserProfile.SUCCESS:
      const info = (action.payload as any) as APIUserProfileWitTools
      return {
        ...state,
        info,
        isFetching: false
      }
    default:
      return state
  }
}
