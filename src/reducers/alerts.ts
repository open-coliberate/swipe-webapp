import * as Actions from '../actions'
import { ActionWithPayload, AlertModel } from '../models'

export const AlertInitState: AlertModel = {
  type: '',
  isTriggered: false,
  text: '',
  isFatal: false
}

export function alertReducer(
  state: AlertModel = AlertInitState,
  action: ActionWithPayload
): AlertModel {
  switch (action.type) {
    case Actions.collapseAlert.PERFORM:
      return {
        ...state,
        type: '',
        isTriggered: false,
        text: ''
      }
    case Actions.triggerAlert.PERFORM:
      return {
        ...state,
        isTriggered: true
      }
    case Actions.fatalAlert.PERFORM:
      alert('Fatal error!\n' + action.payload)
      return {
        ...state,
        isFatal: true
      }
    default:
      return state
  }
}
