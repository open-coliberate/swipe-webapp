import { BookingDtoExtended } from './../../pages/Calendars/CalendarScheduler'
import { Action } from 'redux-actions'

import * as Actions from '../../actions'
import { RoutineModel } from '../../models'

const initialState: BookingDtoExtended = {
  timeSlot: {
    from: 0,
    to: 0
  },
  bookingDate: '',
  ownerId: '',
  toolId: '',
  eventStatus: '',
  buddyEmail: '',
  workingTime: [],
  isPosting: false
}

export default function bookingsItem(
  state: BookingDtoExtended = initialState,
  action: Action<RoutineModel>
): BookingDtoExtended {
  switch (action.type) {
    case Actions.createBooking.PERFORM:
      const { bookingDate, ownerId, timeSlot, toolId, workingTime } = action.payload as any
      return {
        ...state,
        bookingDate,
        ownerId,
        timeSlot,
        toolId,
        workingTime
      }
    case Actions.postBookingRoutineActions.TRIGGER:
      return { ...state, isPosting: true }
    case Actions.postBookingRoutineActions.FULFILL:
    case Actions.resetBooking.PERFORM:
      return {
        ...initialState
      }
    default:
      return state
  }
}
