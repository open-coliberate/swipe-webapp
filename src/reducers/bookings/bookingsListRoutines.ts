import { Action } from 'redux-actions'

import * as Actions from '../../actions'
import { APIBooking, RoutineModel, BookingsListExtended } from '../../models'

const initialState: BookingsListExtended = {
  data: [],
  isFetching: false
}

export default function bookingsListRoutines(
  state: BookingsListExtended = initialState,
  action: Action<RoutineModel>
): BookingsListExtended {
  switch (action.type) {
    case Actions.bookingsListRoutineActions.TRIGGER:
      return {
        ...state,
        isFetching: true
      }
    case Actions.postBookingRoutineActions.SUCCESS:
      const { response } = action.payload as any
      return {
        ...state,
        data: state.data.concat(response)
      }
    case Actions.bookingsListRoutineActions.SUCCESS:
      const data = (action.payload as any).response as APIBooking[]
      return {
        ...state,
        data
      }
    case Actions.bookingsListRoutineActions.FAILURE:
    case Actions.bookingsListRoutineActions.FULFILL:
      return {
        ...state,
        isFetching: false
      }
    default:
      return state
  }
}
