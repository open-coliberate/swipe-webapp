import { Action } from 'redux-actions'
import format from 'date-fns/format'

import * as Actions from '../../actions'
import { RoutineModel } from '../../models'

const initialState: string = format(new Date(), 'YYYY-MM-DD')

export default function bookingDate(
  state: string = initialState,
  action: Action<RoutineModel>
): string {
  switch (action.type) {
    case Actions.setBookingDate.PERFORM:
      return action.payload as string
    default:
      return state
  }
}
