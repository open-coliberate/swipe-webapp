import { Action } from 'redux-actions'

import * as Actions from '../../actions'
import { APITool, ToolsListExtended, RoutineModel } from '../../models'

const initialState: ToolsListExtended = {
  data: [],
  isFetching: false
}

export default function toolsListRoutines(
  state: ToolsListExtended = initialState,
  action: Action<RoutineModel>
): ToolsListExtended {
  switch (action.type) {
    case Actions.toolsListRoutineActions.TRIGGER:
      return {
        ...state,
        isFetching: true
      }
    case Actions.toolsListRoutineActions.SUCCESS:
      const data = (action.payload as any).response as APITool[]
      return {
        ...state,
        data
      }
    case Actions.toolsListRoutineActions.FAILURE:
    case Actions.toolsListRoutineActions.FULFILL:
      return {
        ...state,
        isFetching: false
      }
    default:
      return state
  }
}
