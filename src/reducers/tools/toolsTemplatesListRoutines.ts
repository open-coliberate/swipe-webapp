import { APITemplate } from './../../models/APIModels'
import { Action } from 'redux-actions'

import * as Actions from '../../actions'
import { ToolsTemplatesListExtended, RoutineModel } from '../../models'

const initialState: ToolsTemplatesListExtended = {
  data: [],
  isFetching: false
}

export default function toolsTemplatesListRoutines(
  state: ToolsTemplatesListExtended = initialState,
  action: Action<RoutineModel>
): ToolsTemplatesListExtended {
  switch (action.type) {
    case Actions.toolsTemplatesListRoutineActions.TRIGGER:
      return {
        ...state,
        isFetching: true
      }
    case Actions.toolsTemplatesListRoutineActions.SUCCESS:
      const data = (action.payload as any).response as APITemplate[]
      return {
        ...state,
        data
      }
    case Actions.toolsTemplatesListRoutineActions.FAILURE:
    case Actions.toolsTemplatesListRoutineActions.FULFILL:
      return {
        ...state,
        isFetching: false
      }
    default:
      return state
  }
}
