import { Action } from 'redux-actions'
// import groupBy from 'lodash/groupBy'

import * as Actions from '../../actions'
import { APISpace, RoutineModel, SpacesListExtended } from '../../models'

const initialState: SpacesListExtended = {
  data: [],
  isFetching: false
}

export default function spacesListRoutines(
  state: SpacesListExtended = initialState,
  action: Action<RoutineModel>
): SpacesListExtended {
  switch (action.type) {
    case Actions.spacesListRoutineActions.TRIGGER:
      return {
        ...state,
        isFetching: true
      }
    case Actions.spacesListRoutineActions.SUCCESS:
      const spaces = (action.payload as any).response as APISpace[]
      const data = spaces
        .filter(el => el.hub)
        .map((el: any) => {
          const wh = el.workingHours.map((item: any) => ({
            ...item,
            from: item.from / 3600,
            to: item.to / 3600
          }))
          return {
            ...el,
            workingHours: wh
          }
        })
      return {
        ...state,
        data
      }
    case Actions.spacesListRoutineActions.FAILURE:
    case Actions.spacesListRoutineActions.FULFILL:
      return {
        ...state,
        isFetching: false
      }
    default:
      return state
  }
}
