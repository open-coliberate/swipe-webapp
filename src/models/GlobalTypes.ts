import { AxiosResponse } from 'axios'

export interface ActionWithPayload {
  type: string
  payload: AxiosResponse<HttpError>
}

export interface ActionWithStringPayload {
  type: string
  payload: string
}

export interface HttpError {
  statusCode: number
  error: string
  message: string
}

export interface TokenModel {
  accessToken: string
  refreshToken: string
  expiresIn: string
}

export interface SelectOption {
  value: any
  label: string
}

export interface SelectOption {
  value: any
  label: string
}
