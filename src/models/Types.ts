export enum Roles {
  OWNER = 'owner',
  SUPER_USER = 'super-user',
  GENERAL_USER = 'general',
  LAB_MANAGER = 'lab-manager'
}

export enum RecurringTypes {
  'PER_WEEK' = 'per_week',
  'PER_MONTH' = 'per_month',
  'ONE_TIME' = 'one_time'
}
