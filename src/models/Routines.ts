export enum RoutineModel {
  TRIGGER = 'TRIGGER',
  REQUEST = 'REQUEST',
  SUCCESS = 'SUCCESS',
  FAILURE = 'FAILURE',
  FULFILL = 'FULFILL'
}

export enum TrivialActionsModel {
  PERFORM = 'PERFORM'
}
