import { APISpace } from './APIModels'

export interface SpacesListExtended {
  data: APISpace[]
  isFetching: boolean
}

export interface SpaceExtended {
  data: APISpace
  isFetching: boolean
}
