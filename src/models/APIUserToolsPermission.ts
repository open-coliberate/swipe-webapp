import { ToolInfo } from './Tools'

export type APIUserToolsPermission = {
  allowed: ToolInfo[]
  notAllowed: ToolInfo[]
}
