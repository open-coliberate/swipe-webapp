import { APIEventBooking } from './APIModels'

// TODO: remove after API was fixed CLBRT-127
export interface APIEventBookingExt extends APIEventBooking {
  spaceID: string
}

export type EventsListReducer = {
  list: APIEventBookingExt[]
  isFetching: boolean
}
