import { TokenModel } from './GlobalTypes'
import { Roles } from './Types'
export interface APIUserFull {
  createdAt: string
  email: string
  firstName: string
  id: string
  lastName: string
  role: Roles
  token: TokenModel
  description: string
  isActive: boolean
  organization: {
    id: string
    title: string
  }
  phone: string
  photo: string | null
  updatedAt: string
}

export interface APIUserShort {
  id: string
  firstName: string
  lastName: string
  email: string
  description: string
  role: string
  organization: {
    id: string
    title: string
  }
  photo: string | null
}

export interface APITemplate {
  availableInWorkingHours: boolean
  buddyMode: boolean
  updatedAt: string
  createdAt: string
  description?: string
  id: string
  maxWorkTime?: number
  minWorkTime?: number
  organization: APIOrganization
  requiresTraining: boolean
  sound: boolean
  title: string
  tools?: string[]
}

export interface APITool {
  controller?: APIController
  title: string
  id: string
  template: APITemplate
  updatedAt: string
  createdAt: string
}

export interface APIError {
  errors: string[]
  message: string
}

export interface APIBooking {
  bookingDate: string
  buddy?: string
  createdAt: string
  updatedAt: string
  id: string
  owner: {
    id: string
    ownerFirstName: string
    ownerLastName: string
  }
  status: string
  timeSlot: {
    to: number
    from: number
  }
  tool: APITool
}

export interface APIOrganization {
  updatedAt: string
  createdAt: string
  id: string
  title: string
}

export interface APISpace {
  id: string
  updatedAt: string
  createdAt: string
  title: string
  description: string
  hub: APIHub
  workingHours: APIWorkingHours[]
}

export interface APIHubCreateResponse {
  id: string
  createdAt: string
  updatedAt: string
  title: string
}
export interface APIHub extends APIHubCreateResponse {
  address?: string
  organization: APIOrganization
}

export interface APIWorkingHours {
  day: number
  isWorking: boolean
  hours: Array<{
    from: number
    to: number
  }>
}

export interface APIController {
  updatedAt: string
  createdAt: string
  id: string
  token: string
  space: APISpace
}

// DTOs

export interface SpaceFormDto {
  title: string
  hubs: {
    label: string
    value: string
  }
  description?: string
  maxWorkTime: number
  minWorkTime: number
}

export interface APIEventBooking {
  timeSlot: {
    from: number
    to: number
  }
  toolIds: string[]
  title?: string
  recurring: string
  eventDate: string
  spaceId: string
}
