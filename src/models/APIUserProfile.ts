import { APIUserToolsPermission } from './APIUserToolsPermission'

export type APIUserProfile = {
  createdAt: string
  description?: string
  email: string
  firstName: string
  id: string
  isActive: boolean
  lastName: string
  organization: { id: string; title: string }
  phone?: string
  photo?: string
  role: string
  updatedAt: string
}

export type APIUserProfileWitTools = APIUserProfile & {
  toolsPermission: APIUserToolsPermission | null
}
