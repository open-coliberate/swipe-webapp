import { APIUserFull } from './APIModels'
import { APIUserToolsPermission } from './APIUserToolsPermission'
import { TokenModel } from './GlobalTypes'

export interface UserShort {
  id: string
  firstName: string
  lastName: string
  email: string
  description?: string
  phone?: string
  token: TokenModel
  isFetching: boolean
}

export interface UserFullExtended extends APIUserFull {
  toolsPermission: APIUserToolsPermission | null
  isToSetNewPass: boolean
  isUserAuth: boolean
  isFetching: boolean
}
