export type APIWorkingHoursType = {
  day: 0 | 1 | 2 | 3 | 4 | 5 | 6
  from: number
  to: number
  id: null | string
  isWorking: true
  space: string
  deleted: boolean
}
