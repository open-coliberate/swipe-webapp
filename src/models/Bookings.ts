import { APIBooking } from './APIModels'

export interface BookingsListExtended {
  data: APIBooking[]
  isFetching: boolean
}
