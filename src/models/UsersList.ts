import { APIUserProfile } from './APIUserProfile'

export interface UsersListReducer {
  list: APIUserProfile[]
  totalUsers: number
  isFetching: boolean
}
