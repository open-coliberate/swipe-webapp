import { APIHub } from './APIModels'

export interface HubsListExtended {
  data: APIHub[]
  isFetching: boolean
}

export interface HubsExtended extends HubsListExtended {
  currHub: string
}
