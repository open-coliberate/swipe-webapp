import { APITool, APITemplate } from './APIModels'

export type ToolInfo = {
  id: string
  createdAt: string
  updatedAt: string
  title: string
  description: string
  minWorkTime: number
  maxWorkTime: number
  buddyMode: boolean
  availableInWorkingHours: boolean
  requiresTraining: boolean
  sound: boolean
  organization: {
    id: string
    createdAt: string
    updatedAt: string
    title: string
  }
}

export interface ToolsListExtended {
  data: APITool[]
  isFetching: boolean
}

export interface ToolsTemplatesListExtended {
  data: APITemplate[]
  isFetching: boolean
}
