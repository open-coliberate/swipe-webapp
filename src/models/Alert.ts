export interface AlertModel {
  type: string
  isTriggered: boolean
  text: string
  isFatal: boolean
}
