import axios from 'axios'
import Cookies from 'js-cookie'

axios.defaults.baseURL = 'https://dev-brainbean-api.herokuapp.com/api/v1/' // TODO: this link should be changed, and should be according to env

export const axiosInstance = axios.create({
  headers: {
    Authorization: `Bearer *token*`,
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

axiosInstance.interceptors.request.use(
  config => {
    const accessToken = Cookies.get('accessToken')
    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`
    }
    return config
  },
  error => Promise.reject(error)
)
