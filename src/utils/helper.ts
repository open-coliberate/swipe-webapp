import get from 'lodash/get'

import { SchedulerEventModel } from './../pages/Calendars/CalendarScheduler'
import { FormBookingValues } from './../pages/Calendars/CalendarModal'
import {
  APIBooking,
  APIUserProfileWitTools,
  APITool,
  APISpace,
  APIEventBooking,
  BookingsListExtended,
  ToolsListExtended,
  APIEventBookingExt,
  ToolInfo
} from '../models'
import { BookingDto } from '../routines/bookings/bookingsPost'
import { UserInfo } from '../components/ProfileForm'
import { OptionsProps } from '../components/FormTools/Tool'

export const getDocHeight = (): number => {
  const D = document
  return Math.max(
    D.body.scrollHeight,
    (D.documentElement as HTMLElement).scrollHeight,
    D.body.offsetHeight,
    (D.documentElement as HTMLElement).offsetHeight,
    D.body.clientHeight,
    (D.documentElement as HTMLElement).clientHeight
  )
}

export const isWindowScrollAtBottom = (): boolean | void => {
  let winheight = window.innerHeight || (document.documentElement || document.body).clientHeight
  let docheight = getDocHeight()
  let scrollTop = window.pageYOffset
  let trackLength = docheight - winheight
  let pctScrolled = Math.floor((scrollTop / trackLength) * 100)

  if (pctScrolled >= 95) return true
}

export const cutWordsFromString = (strToCut: string, excludedWords: string[]): string => {
  let arrOfWords = strToCut.split(' ')
  arrOfWords = arrOfWords.map(word => (word.includes('#') ? word.substring(1) : word))
  arrOfWords = arrOfWords.filter(word => word !== excludedWords.find(ex => ex === word))
  return arrOfWords.join(' ')
}

export const email = (value: string) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined

export const required = (value: string) => (value ? undefined : 'Required')

export const booleanToReadable = (value: boolean) => (value ? 'On' : 'Off')

export const transformBookingToEvent = (
  toolsBooking: (APIBooking | APIEventBookingExt)[]
): SchedulerEventModel[] => {
  const output = toolsBooking.reduce((prev: SchedulerEventModel[], curr: APIBooking) => {
    const isSpace = 'spaceID' in curr
    const dateKey = isSpace ? 'eventDate' : 'bookingDate'
    const start = curr[dateKey].slice(0, 10).concat(' ', String(curr.timeSlot.from), ':00:00')
    const end = curr[dateKey].slice(0, 10).concat(' ', String(curr.timeSlot.to), ':00:00')
    const event: SchedulerEventModel = {
      id: curr.id,
      start,
      end,
      type: isSpace ? 2 : 1,
      resourceId: isSpace ? get(curr, 'spaceID', '') : curr.tool.id,
      title: isSpace
        ? get(curr, 'title', 'New event')
        : curr.owner.ownerFirstName.concat(' ', curr.owner.ownerLastName),
      movable: false,
      startResizable: false,
      endResizable: false
    }
    return prev.concat(event)
  }, [])
  return output
}

export const valueSelectPrettier = (value?: string | number): TimeLabel | null => {
  if (value === undefined) return null

  const label = +value > 12 ? `${+value % 12} PM` : `${+value} AM`

  return {
    value: String(value),
    label
  }
}

export interface TimeLabel {
  value: string
  label: string
}

export const timeHourGenerator = (): Array<TimeLabel> => {
  let options = []

  for (let i = 0; i < 24; i++) {
    const labelValue = i === 12 ? 12 : i % 12
    options.push({
      value: `${i}`,
      label: `${labelValue} ${i < 13 ? 'AM' : 'PM'}`
    })
  }
  return options
}

interface WorkingHoursInterval {
  from: TimeLabel
  to?: TimeLabel
}
interface Acm {
  prev: number
  range: WorkingHoursInterval[]
}
export const timeRangeFormatter = (arr: Array<TimeLabel>): WorkingHoursInterval[] => {
  const result = arr.reduce(
    (acm: Acm, hour: TimeLabel, idx: number, arr: Array<TimeLabel>) => {
      if (!idx) {
        acm.range.push({
          from: hour
        })
      }

      const plusOne = idx + 1

      if (arr.length !== plusOne && 1 + Number(hour.value) !== Number(arr[plusOne].value)) {
        acm.range[acm.prev] = {
          ...acm.range[acm.prev],
          to: valueSelectPrettier(Number(hour.value) + 1) || undefined
        }
        acm.range.push({
          from: arr[plusOne]
        })
        acm.prev = acm.range.length - 1
      }

      if (arr.length === plusOne) {
        acm.range[acm.prev] = {
          ...acm.range[acm.prev],
          to: valueSelectPrettier(Number(arr[idx].value) + 1) || undefined
        }
        return acm
      }

      return acm
    },
    {
      prev: 0,
      range: []
    }
  )

  return result.range
}

const toolsFormat = (tools: ToolInfo[], value: boolean): OptionsProps[] | undefined => {
  return tools.length
    ? tools.map(({ id, title, description }) => ({
        id,
        title,
        description,
        value
      }))
    : undefined
}

export const getInitValueForm = (
  userData: APIUserProfileWitTools | null | undefined
): UserInfo | undefined => {
  if (!userData) return undefined
  const toolsAllowed = get(userData, 'toolsPermission.allowed', [])
  const toolsDisallowed = get(userData, 'toolsPermission.notAllowed', [])

  return {
    id: userData.id,
    firstName: userData.firstName,
    lastName: userData.lastName,
    phone: userData.phone,
    email: userData.email,
    description: userData.description,
    password: '',
    passwordConfirmation: '',
    toolsAllowed: toolsFormat(toolsAllowed, true),
    toolsDisallowed: toolsFormat(toolsDisallowed, false),
    workingHours: []
  }
}

interface ToolSelectProps {
  spaceID: string
  tools: (APITool | APISpace)[]
}

export const toolsSelectFormatter = ({ spaceID, tools }: ToolSelectProps) =>
  tools
    .filter(tool => get(tool, 'controller.space.id', null) === spaceID)
    .map(el => ({
      value: el.id,
      label: el.title
    }))

interface EditEventOptions {
  eventType: string
  isSpace: boolean
  schedulerData: any
  spaceTitle: string
  slotId: string
  slotName: string
  start: string
  end: string
}

export const bookingToolPayloadFormat = ({
  editEventOptions,
  formData,
  ownerId
}: {
  editEventOptions: EditEventOptions
  formData: FormBookingValues
  ownerId: string
}): BookingDto => {
  const bookingDate = new Date(editEventOptions.start).toISOString()
  return {
    timeSlot: {
      from: Number(get(formData, 'from.value', '00')),
      to: Number(get(formData, 'to.value', '24'))
    },
    bookingDate,
    toolId: editEventOptions.slotId,
    buddyEmail: formData.buddyEmail,
    ownerId
  }
}

export const bookingSpacePayloadFormat = ({
  editEventOptions,
  formData
}: {
  editEventOptions: EditEventOptions
  formData: FormBookingValues
}): APIEventBooking => {
  const bookingDate = new Date(editEventOptions.start).toISOString()
  return {
    timeSlot: {
      from: Number(get(formData, 'from.value', '00')),
      to: Number(get(formData, 'to.value', '24'))
    },
    toolIds: formData.toolIds,
    title: formData.title,
    recurring: formData.recurring.value,
    eventDate: bookingDate,
    spaceId: editEventOptions.slotId
  }
}

type GroupInstanceProps = {
  spaces: APISpace[]
  bookingsArr: BookingsListExtended
  toolsArr: ToolsListExtended
  eventsArr: APIEventBookingExt[]
}

export const groupInstance = ({ spaces, bookingsArr, toolsArr, eventsArr }: GroupInstanceProps) => {
  return spaces.reduce(
    (prev: { bookings: APIBooking[]; tools: [APISpace, APITool] }, space) => {
      const bookingsForSpace = bookingsArr.data.filter(
        booking => get(booking, 'tool.controller.space.id', null) === space.id
      )
      const toolsForSpace = toolsArr.data.filter(
        tool => get(tool, 'controller.space.id', null) === space.id
      )
      const eventsForSpace = eventsArr.filter(event => event.spaceID === space.id)
      return {
        bookings: [...prev.bookings, ...bookingsForSpace, ...eventsForSpace],
        tools: prev.tools.concat(space, toolsForSpace)
      }
    },
    {
      bookings: [],
      tools: []
    }
  )
}
