import { all, fork } from 'redux-saga/effects'

import { saga as TokenSaga } from '../routines/user/refreshToken'
import { saga as InfoSaga } from '../routines/user/userInfo'
import { saga as LoginSaga } from '../routines/user/userLogin'
import { saga as NewPassSaga } from '../routines/user/userNewPass'
import { saga as PassRecoverySaga } from '../routines/user/userPassRecovery'
import { saga as UpdateInfoSaga } from '../routines/user/updateInfo'
import { saga as ChangePassSaga } from '../routines/user/userChangePass'
import { saga as getUserTemplatesPermission } from '../routines/user/getUserTemplatesPermission'
import { saga as changeAllowableTools } from '../routines/user/changeAllowableTools'
import { saga as setUserWorkingHours } from '../routines/user/setUserWorkingHours'

export default function* root() {
  return yield all([
    fork(LoginSaga),
    fork(PassRecoverySaga),
    fork(NewPassSaga),
    fork(InfoSaga),
    fork(TokenSaga),
    fork(UpdateInfoSaga),
    fork(ChangePassSaga),
    fork(getUserTemplatesPermission),
    fork(changeAllowableTools),
    fork(setUserWorkingHours)
  ])
}
