import { all, fork } from 'redux-saga/effects'

import { saga as getUsersList } from '../routines/usersList/getUsersList'
import { saga as addNewUsers } from '../routines/usersList/addNewUsers'
import { saga as promoteUser } from '../routines/usersList/promoteUser'

export default function* root() {
  return yield all([fork(getUsersList), fork(addNewUsers), fork(promoteUser)])
}
