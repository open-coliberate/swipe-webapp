import { all, fork } from 'redux-saga/effects'
import { saga as getUserProfile } from '../routines/profile/getUserProfile'

export default function* root() {
  return yield all([fork(getUserProfile)])
}
