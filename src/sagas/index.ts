import { all, fork } from 'redux-saga/effects'

import ToolsSaga from './tools'
import UserSaga from './user'
import BookingsSaga from './bookings'
import HubsSaga from './hubs'
import SpacesSaga from './spaces'
import UsersList from './usersList'
import EventsSaga from './events'
import ProfileSaga from './profile'

export default function* root() {
  return yield all([
    fork(UserSaga),
    fork(ToolsSaga),
    fork(BookingsSaga),
    fork(HubsSaga),
    fork(SpacesSaga),
    fork(UsersList),
    fork(EventsSaga),
    fork(ProfileSaga)
  ])
}
