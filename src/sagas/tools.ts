import { all, fork } from 'redux-saga/effects'

import { saga as GetToolsSaga } from '../routines/tools/toolsList'
import { saga as ToolsTemplatesSaga } from '../routines/tools/toolsTemplatesList'
import { saga as ToolsNewSaga } from '../routines/tools/toolsTemplateNew'

export default function* root() {
  return yield all([fork(GetToolsSaga), fork(ToolsTemplatesSaga), fork(ToolsNewSaga)])
}
