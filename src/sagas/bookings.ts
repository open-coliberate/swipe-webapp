import { all, fork } from 'redux-saga/effects'

import { saga as getBookingsSaga } from '../routines/bookings/bookingsList'
import { saga as newBookingSaga } from '../routines/bookings/bookingsPost'

export default function* root() {
  return yield all([fork(getBookingsSaga), fork(newBookingSaga)])
}
