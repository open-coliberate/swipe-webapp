import { all, fork } from 'redux-saga/effects'

import { saga as GetHubsSaga } from '../routines/hubs/hubsList'
import { saga as createHubSaga } from '../routines/hubs/createHub'
import { saga as deleteHubSaga } from '../routines/hubs/deleteHub'

export default function* root() {
  return yield all([fork(GetHubsSaga), fork(createHubSaga), fork(deleteHubSaga)])
}
