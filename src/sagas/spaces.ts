import { all, fork } from 'redux-saga/effects'

import { saga as GetSpacesSaga } from '../routines/spaces/spacesList'
import { saga as postSpaceSaga } from '../routines/spaces/newSpace'

export default function* root() {
  return yield all([fork(GetSpacesSaga), fork(postSpaceSaga)])
}
