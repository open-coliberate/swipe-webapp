import { all, fork } from 'redux-saga/effects'

import { saga as postBookingEvent } from '../routines/events/bookingEvent'
import { saga as getBookingEventList } from '../routines/events/getBookingEventList'

export default function* root() {
  return yield all([fork(postBookingEvent), fork(getBookingEventList)])
}
