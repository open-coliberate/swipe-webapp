import '../Login/style.css'

import Cookies from 'js-cookie'
import * as React from 'react'
import { Redirect } from 'react-router-dom'
import { Field, InjectedFormProps, reduxForm } from 'redux-form'
import { bindRoutineToReduxForm } from 'redux-saga-routines'

import { recoverPassRoutineActions } from '../../actions'
import Button from '../../components/Button'
import FormInput from '../../components/FormInput'
import { UserShort } from '../../models'
import { Payload } from '../../routines/user/userLogin'
import { required } from '../../utils/helper'
import { Routes } from '../../utils/routes'

type LoginProps = InjectedFormProps<Payload> & UserShort

class PassRecovery extends React.PureComponent<LoginProps> {
  submitForm = bindRoutineToReduxForm(recoverPassRoutineActions)

  componentDidMount() {
    this.props.reset()
  }

  render() {
    const hasCookie = !!Cookies.get('token')
    const { handleSubmit } = this.props
    if (hasCookie) {
      return <Redirect to={Routes.Home} />
    } else {
      return (
        <div className='login-container'>
          <form className='login-form' onSubmit={handleSubmit(this.submitForm)}>
            <Field
              name='email'
              placeholder='Email adderss'
              className='login-form-input'
              component={FormInput}
              validate={required}
            />
            <Button
              disabled={this.props.submitting || this.props.invalid}
              className='login-form-btn'
              type='submit'
              children='Retrieve password'
            />
          </form>
        </div>
      )
    }
  }
}

export default reduxForm<Payload>({
  form: 'pass-recovery'
})(PassRecovery)
