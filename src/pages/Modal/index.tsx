import React, { PureComponent } from 'react'
import { createPortal } from 'react-dom'
import classnames from 'classnames'
import './style.css'

export interface ModalParentProps {
  closeModal: () => void
  children: React.ReactNode
  className?: '--dark'
  arrow?: boolean
  shouldCalculatePosition?: boolean
  coordinates?: {
    x: number
    y: number
  }
}

export interface ModalState {
  coordinateX: number
  transform: boolean
}

const modalRoot: HTMLElement | null = document.getElementById('modal')

class Modal extends PureComponent<ModalParentProps, ModalState> {
  private divRef: React.RefObject<HTMLInputElement>

  static defaultProps = {
    shouldCalculatePosition: false,
    arrow: false,
    coordinates: null
  }

  constructor(props: ModalParentProps) {
    super(props)
    this.divRef = React.createRef()
    this.state = {
      coordinateX: props.coordinates ? props.coordinates.x : 0,
      transform: true
    }
  }

  componentDidMount() {
    if (this.props.shouldCalculatePosition) {
      const div: HTMLElement | null = this.divRef.current
      const parent: HTMLElement | null = div && div.parentElement
      const { coordinates } = this.props

      if (div && parent && coordinates) {
        const { width } = div.getBoundingClientRect()
        const enoughPlace = parent.offsetWidth > coordinates.x + width / 2
        const coordinateX = enoughPlace ? 0 : parent.offsetWidth - width - 3
        !enoughPlace && this.setState({ coordinateX, transform: false })
      }
    }
  }

  clickHandler = (e: React.MouseEvent<HTMLElement>) => {
    const { target } = e as any

    if (target.classList.contains('modal-container')) {
      e.preventDefault()
      this.props.closeModal()
    }
  }

  calculatePosition = (): { styleArrow: object; styleContainer: object } => {
    const { coordinates, shouldCalculatePosition } = this.props
    const styleArrow =
      shouldCalculatePosition && coordinates
        ? {
            left: `${coordinates.x}px`,
            top: `${coordinates.y}px`
          }
        : {}
    const styleContainer =
      shouldCalculatePosition && coordinates
        ? {
            left: `${this.state.coordinateX}px`,
            top: `${coordinates.y + 5}px`,
            transform: this.state.transform ? 'translate(-50%, 0)' : 'none'
          }
        : {}

    return { styleArrow, styleContainer }
  }

  render() {
    const { styleArrow, styleContainer } = this.calculatePosition()
    const cn = classnames('modal-container', this.props.className)

    return this.props.children && modalRoot
      ? createPortal(
          <div className={cn} onClick={this.clickHandler}>
            {this.props.arrow ? <i className='modal-arrow' style={styleArrow} /> : null}
            <div className='modal-content' style={styleContainer} ref={this.divRef}>
              {this.props.children}
            </div>
          </div>,
          modalRoot
        )
      : null
  }
}

export default Modal
