import * as React from 'react'
import { RouteComponentProps, withRouter } from 'react-router'
import { compose } from 'recompose'
import { InjectedFormProps, reduxForm } from 'redux-form'

import Button from '../../../components/Button'
import { Payload } from '../../../routines/tools/toolsTemplateNew'
import { Routes } from '../../../utils/routes'
import { submitSpace } from './submit'

interface ComponentPropsFromState {
  organizationId: string
}

interface ToolsActionsState {
  shouldRedirect: boolean
  shouldReset: boolean
}

type ToolActionsProps = InjectedFormProps<Payload> &
  RouteComponentProps<void> &
  ComponentPropsFromState

class ToolActions extends React.Component<ToolActionsProps, ToolsActionsState> {
  constructor(props: ToolActionsProps) {
    super(props)
    this.state = {
      shouldRedirect: false,
      shouldReset: false
    }
  }

  private handleCreate = () => {
    this.setState({
      shouldRedirect: true
    })
  }

  private handleSaveAndNew = () => {
    this.setState({
      shouldReset: true
    })
  }

  private handleClose = () => {
    this.props.history.replace(Routes.Calendars)
  }

  getSnapshotBeforeUpdate() {
    if (this.props.submitSucceeded && this.state.shouldRedirect) {
      this.props.history.replace(Routes.NewSpace)
    }

    if (this.props.submitSucceeded && this.state.shouldReset) {
      this.props.reset()
    }
    return null
  }

  render() {
    const { handleSubmit, submitting, invalid } = this.props
    const isDisabled = submitting || invalid

    return (
      <form className='space-actions' onSubmit={handleSubmit}>
        <Button disabled={submitting} className='space-form-btn --grey' onClick={this.handleClose}>
          Cancel
        </Button>
        <Button
          className='btn-small --grey'
          disabled={isDisabled}
          icoPosition='after'
          icoType='plus'
          onClick={this.handleCreate}
        >
          Save and add another space
        </Button>
        <Button
          disabled={isDisabled}
          className='space-form-btn'
          type='submit'
          onClick={this.handleSaveAndNew}
        >
          Create Space
        </Button>
      </form>
    )
  }
}

export default compose(
  reduxForm<Payload>({
    form: 'new-space',
    onSubmit: submitSpace
  }),
  withRouter
)(ToolActions)
