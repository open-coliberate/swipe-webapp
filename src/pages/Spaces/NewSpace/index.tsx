import './style.css'

import * as React from 'react'
import { RouteComponentProps } from 'react-router'

import SpaceActions from './NewSpaceActions'
import SpaceDevices from './NewSpaceDevices'
import SpaceForm from './NewSpaceForm'
import { HeaderPageContent } from '../../../components/Headers'
import { Routes } from '../../../utils/routes'

export interface NewSpaceProps {}

type NewToolTemplateProps = RouteComponentProps<void> & NewSpaceProps

export class NewSpace extends React.PureComponent<NewToolTemplateProps> {
  render() {
    return (
      <div className='page-container'>
        <HeaderPageContent
          title='Create new Space'
          linkTitle='Back to Calendar'
          linkURL={Routes.Calendars}
        />
        <div className='page-content space'>
          <SpaceForm />
          <SpaceDevices />
          <SpaceActions />
        </div>
      </div>
    )
  }
}
