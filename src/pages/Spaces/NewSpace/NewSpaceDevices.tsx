import * as React from 'react'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import { AnyAction, Dispatch } from 'redux'
import { Field, InjectedFormProps, reduxForm } from 'redux-form'

import { hubsListRoutineActions } from '../../../actions'
import FormSelect from '../../../components/FormSelect'
import { APIHub } from '../../../models'
import { RootState } from '../../../reducers'
import { Payload } from '../../../routines/tools/toolsTemplateNew'
import { required } from '../../../utils/helper'
import { submitSpace } from './submit'

interface ComponentsPropsFromDispatch {
  fetchHubs: () => void
}

interface ComponentPropsFromState {
  data: APIHub[]
  isFetching: boolean
}

type ToolDevicesProps = InjectedFormProps<Payload> &
  ComponentsPropsFromDispatch &
  ComponentPropsFromState

class ToolDevices extends React.Component<ToolDevicesProps> {
  static mapStateToProps(state: RootState): ComponentPropsFromState {
    return {
      data: state.hubs.data,
      isFetching: state.hubs.isFetching
    }
  }

  static mapDispatchToProps(dispatch: Dispatch<AnyAction>): ComponentsPropsFromDispatch {
    return {
      fetchHubs: () => dispatch(hubsListRoutineActions())
    }
  }

  componentDidMount() {
    this.props.fetchHubs()
  }

  render() {
    const options = this.props.data.map(el => ({
      value: el.id,
      label: el.title
    }))
    return (
      <div className='container container__multy'>
        <Field
          name='hubs'
          className='space-form-input space-form-input__multiselect form-field'
          isClearable
          options={options}
          component={FormSelect}
          validate={required}
        />
      </div>
    )
  }
}

export default compose(
  reduxForm<Payload>({
    form: 'new-space',
    onSubmit: submitSpace
  }),
  connect(
    ToolDevices.mapStateToProps,
    ToolDevices.mapDispatchToProps
  )
)(ToolDevices)
