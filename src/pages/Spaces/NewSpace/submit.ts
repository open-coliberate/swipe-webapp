import { bindRoutineToReduxForm } from 'redux-saga-routines'

import { newSpaceRoutineActions } from '../../../actions'

export const submitSpace = bindRoutineToReduxForm(newSpaceRoutineActions)
