import * as React from 'react'
import { RouteComponentProps, withRouter } from 'react-router'
import { compose } from 'recompose'

import { HeaderPageContent } from '../../../components/Headers'
import { Routes } from '../../../utils/routes'

// import SpaceActions from './NewSpaceActions';
// import SpaceDevices from './NewSpaceDevices';
// import SpaceForm from './NewSpaceForm';

type SpaceProps = RouteComponentProps<void>

class Space extends React.PureComponent<SpaceProps> {
  render() {
    return (
      <div className='space-container'>
        <HeaderPageContent title='Space' linkURL={Routes.Calendars} linkTitle='Back to Calendar' />
        <div className='space'>
          {/* <SpaceForm />
          <SpaceDevices />
          <SpaceActions /> */}
          123123
        </div>
      </div>
    )
  }
}

export default compose(withRouter)(Space)
