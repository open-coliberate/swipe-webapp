import Cookies from 'js-cookie'
import React from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { Redirect, Route, Switch } from 'react-router-dom'

import Loader from '../../components/Loader'
import { RootState } from '../../reducers/state'
import { Routes } from '../../utils/routes'
import Admins from '../Admins'
import Analytics from '../Analytics'
import Calendar from '../Calendars'
import UserProfile from '../UserProfile'
import { NewSpace } from '../Spaces/NewSpace'
import Space from '../Spaces/Space'
import Tools from '../Tools'
import Users from '../Users'
import Hub from '../Hub'
import { infoRoutineActions, setActivation } from '../../actions'
import { AppHeader } from '../../components/Headers'

interface HomeProps {
  isUserFetching: boolean
  isUserAuth: boolean
  id: string
}

interface ComponentPropsFromDispatch {
  refreshToken: (refreshToken: string) => void
  setToActivate: () => void
  getUserInfo: ({ id }: { id: string }) => void
}

type Props = HomeProps & ComponentPropsFromDispatch & RouteComponentProps<void>

export class Home extends React.Component<Props> {
  static mapStateToProps = ({ user }: RootState): HomeProps => ({
    id: user.id,
    isUserFetching: user.isFetching,
    isUserAuth: user.isUserAuth
  })

  static mapDispatchToProps = {
    getUserInfo: infoRoutineActions,
    setToActivate: setActivation.perform
  }

  componentDidMount() {
    const { id } = this.props
    const myID = Cookies.get('myId')
    if (!id) {
      myID ? this.props.getUserInfo({ id: myID }) : this.props.history.replace(Routes.Login)
    }
  }

  componentDidCatch(err: any) {
    console.log('err:', err)
    this.props.setToActivate()
    this.props.history.replace(Routes.Login)
  }

  render() {
    const isLoaded = !this.props.isUserFetching && this.props.isUserAuth
    return isLoaded ? (
      <React.Fragment>
        <AppHeader />
        <Switch>
          <Route exact path={Routes.Me} component={UserProfile} />
          <Route exact path={Routes.Calendars} component={Calendar} />
          <Route exact path={Routes.NewSpace} component={NewSpace} />
          <Route exact path={Routes.Admins} component={Admins} />
          <Route exact path={Routes.Analytics} component={Analytics} />
          <Route path={Routes.Space} component={Space} />
          <Route path={Routes.Tools} component={Tools} />
          <Route path={Routes.Users} component={Users} />
          <Route path={Routes.HubParam} component={Hub} />
          <Redirect to={Routes.Calendars} />
        </Switch>
      </React.Fragment>
    ) : (
      <Loader />
    )
  }
}

export default connect(
  Home.mapStateToProps,
  Home.mapDispatchToProps
)(Home)
