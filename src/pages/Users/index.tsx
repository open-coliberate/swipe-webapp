import React from 'react'
import { Route, Switch } from 'react-router-dom'
import UsersNew from './UsersNew'
import UsersList from './UsersList'
import UserProfile from '../UserProfile'
import { Routes } from '../../utils/routes'

import './style.css'

const Users: React.StatelessComponent = () => (
  <Switch>
    <Route exact path={Routes.UsersNew} component={UsersNew} />
    <Route exact path={Routes.UserProfileParam} component={UserProfile} />
    <Route path={Routes.UsersPageParam} component={UsersList} />
    <Route component={UsersList} />
  </Switch>
)
export default Users
