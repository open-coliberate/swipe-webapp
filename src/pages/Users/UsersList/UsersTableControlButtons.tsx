import React, { Fragment } from 'react'
import { RouterAction } from 'connected-react-router'

import Button from '../../../components/Button'
import Input from '../../../components/Input'
import { Routes } from '../../../utils/routes'

interface UsersTableControlButtonsProps {
  checkedRow: string[]
  changePage: (location: string) => RouterAction
}

type SRC = React.StatelessComponent<UsersTableControlButtonsProps>

const UsersTableControlButtons: SRC = ({ checkedRow, changePage }) => (
  <Fragment>
    <div className='filter__item'>
      <Button
        className='btn-small --white'
        disabled={checkedRow.length !== 1}
        icoPosition='before'
        icoType='pencil'
        onClick={() => console.log('edit', checkedRow[0])}
      >
        Edit
      </Button>

      <Button
        className='btn-small --white'
        disabled={!checkedRow.length}
        icoPosition='before'
        icoType='bin'
        onClick={() => console.log('Delete', checkedRow)}
      >
        Delete
      </Button>

      <Button
        className='btn-small --darkgrey'
        icoPosition='after'
        icoType='plus'
        onClick={(e: React.SyntheticEvent) => {
          e.preventDefault()
          changePage(Routes.UsersNew)
        }}
      >
        Add new users
      </Button>
    </div>

    <div className='filter__item'>
      <Input
        className='--small --blue'
        placeholder='Search for user by name or email'
        icoPosition='before'
        icoType='search'
      />
    </div>
  </Fragment>
)

export default UsersTableControlButtons
