import React from 'react'
import ReactTable from 'react-table'
import { Link } from 'react-router-dom'

import { PromoteUserProps } from './index'
import { APIUserProfile } from '../../../models'
import Checkbox from '../../../components/Checkbox'
import More from '../../../components/More'
import Footer from '../../../components/TableFooter'
import { Routes } from '../../../utils/routes'

import 'react-table/react-table.css'

const menu = [
  {
    name: 'Add User to Super User',
    role: 'super-user'
  }
]

export interface UsersTableProps extends CommonProps {
  list: APIUserProfile[]
  totalUsers: number
}

interface CommonProps {
  checkedRow: string[]
  handlerCheckboxClick: (a: string) => void
  handlerSelectAll: () => void
  handlerPromoteUser: (arg: PromoteUserProps) => void
}
interface Row {
  original: APIUserProfile
  value: string
}

interface RowArguments extends CommonProps {
  isAllSelected: boolean
}

const renderColumns = ({
  checkedRow,
  handlerCheckboxClick,
  handlerSelectAll,
  handlerPromoteUser,
  isAllSelected
}: RowArguments) => [
  {
    Header: () => <Checkbox value={''} checked={isAllSelected} onClick={handlerSelectAll} />,
    headerClassName: 'th-item td-options',
    accessor: 'id',
    width: 48,
    Cell: (row: Row) => (
      <Checkbox
        value={row.original.id}
        checked={checkedRow.some(id => id === row.value)}
        onClick={(e: React.SyntheticEvent) => {
          e.preventDefault()
          handlerCheckboxClick(row.value)
        }}
      />
    )
  },
  {
    Header: 'Role',
    headerClassName: 'th-item td-options',
    accessor: 'role'
  },
  {
    Header: 'Emails',
    headerClassName: 'th-item td-options',
    accessor: 'email',
    Cell: (row: Row) => (
      <Link to={`${Routes.UserProfile}/${row.original.id}`}>{row.original.email}</Link>
    )
  },
  {
    Header: 'Full Name',
    headerClassName: 'th-item td-options',
    accessor: 'fullName',
    Cell: (row: Row) => (
      <Link to={`${Routes.UserProfile}/${row.original.id}`}>{`${row.original.firstName} ${
        row.original.lastName
      }`}</Link>
    )
  },
  {
    Header: () => <More className='--lite' id='' orgID='' role='' />,
    headerClassName: 'th-item td-options',
    accessor: 'id',
    width: 48,
    Cell: (row: Row) => (
      <More
        menuList={menu}
        id={row.original.id}
        role={row.original.role || ''}
        orgID={row.original.organization.id}
        handlerCallback={handlerPromoteUser}
      />
    )
  }
]

const UsersTable: React.StatelessComponent<UsersTableProps> = props => {
  const { list, checkedRow, handlerPromoteUser } = props
  const isAllSelected = list.length === checkedRow.length
  const options = {
    checkedRow,
    isAllSelected,
    handlerPromoteUser,
    handlerCheckboxClick: props.handlerCheckboxClick,
    handlerSelectAll: props.handlerSelectAll
  }

  return (
    <ReactTable
      columns={renderColumns(options)}
      data={list}
      PaginationComponent={Footer}
      showPagination={Boolean(list.length)}
      sortable={false}
      resizable={false}
      defaultPageSize={list.length}
    />
  )
}

export default UsersTable
