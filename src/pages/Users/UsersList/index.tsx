import React, { Component } from 'react'
import { connect } from 'react-redux'
import { RouterAction } from 'connected-react-router'

import UsersTable from './UsersTable'
import UsersTableControlButtons from './UsersTableControlButtons'
import Loader from '../../../components/Loader'
import { HeaderPageContent } from '../../../components/Headers'
import { Payload as GetUsersListType } from '../../../routines/usersList/getUsersList'
import { Payload as PromoteUserType } from '../../../routines/usersList/promoteUser'
import { UsersListReducer } from '../../../models/UsersList'
import { getUsersListActions, promoteUser } from '../../../actions'
import { Routes } from '../../../utils/routes'

interface ComponentPropsFromDispatch {
  getUsersListActions: (arg: GetUsersListType) => void
  promoteUser: (arg: PromoteUserType) => void
}

interface ComponentPropsFromParent {
  history: {
    replace: (location: string) => RouterAction
    push: (location: string) => RouterAction
  }
  match: {
    params?: {
      page?: string
    }
  }
}

interface ComponentState {
  checkedRow: string[]
}

export interface PromoteUserProps {
  role: string
  id: string
  orgID: string
}

type Props = UsersListReducer & ComponentPropsFromParent & ComponentPropsFromDispatch

class Users extends Component<Props, ComponentState> {
  static mapStateToProps = ({ usersList }: { usersList: UsersListReducer }) => ({
    list: usersList.list,
    totalUsers: usersList.totalUsers,
    isFetching: usersList.isFetching
  })

  static mapDispatchToProps = {
    getUsersListActions,
    promoteUser
  }

  state = {
    checkedRow: []
  }

  handlerCheckboxClick = (val: string) => {
    const { checkedRow } = this.state
    const row = checkedRow.some(id => id === val)
      ? checkedRow.filter(el => el !== val)
      : [...checkedRow, val]
    this.setState({ checkedRow: row })
  }

  handlerSelectAll = () => {
    const { list } = this.props
    const { checkedRow } = this.state
    const row = list.length !== checkedRow.length ? list.map(el => el.id) : []
    this.setState({ checkedRow: row })
  }

  handlerPromoteUser = ({ role, id, orgID }: PromoteUserProps) => {
    const data = { role, organizationId: orgID }
    this.props.promoteUser({ id, data })
  }

  componentDidMount() {
    const { match, history } = this.props
    const getPage = match.params && match.params.page
    const page = Number(getPage) || 1

    if (getPage !== '1') history.replace(`${Routes.UsersPage}/${page}`)
    this.props.getUsersListActions({ page })
  }

  render() {
    const { checkedRow } = this.state
    if (this.props.isFetching) return <Loader />

    return (
      <div className='users page-container'>
        <HeaderPageContent title='Users' />
        <div className='page-content'>
          <div className='page-content__filter'>
            <UsersTableControlButtons
              checkedRow={checkedRow}
              changePage={this.props.history.push}
            />
          </div>
          <UsersTable
            list={this.props.list}
            totalUsers={this.props.totalUsers}
            checkedRow={checkedRow}
            handlerPromoteUser={this.handlerPromoteUser}
            handlerCheckboxClick={this.handlerCheckboxClick}
            handlerSelectAll={this.handlerSelectAll}
          />
        </div>
      </div>
    )
  }
}

export default connect(
  Users.mapStateToProps,
  Users.mapDispatchToProps
)(Users)
