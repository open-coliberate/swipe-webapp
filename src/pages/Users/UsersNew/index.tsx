import React, { Component } from 'react'
import { connect } from 'react-redux'
import UploadFileContainer from './UploadFileContainer'
import Input from '../../../components/Input'
import Button from '../../../components/Button'
import { HeaderPageContent } from '../../../components/Headers'
import { addNewUsers } from '../../../actions'
import { Routes } from '../../../utils/routes'
import { email } from '../../../utils/helper'
import { APIUserFull } from '../../../models/APIModels'
import { Payload } from '../../../routines/usersList/addNewUsers'

import './style.css'

interface UsersNewProps {
  addNewUsers: (arg: Payload) => void
  organizationId: string
}

interface UsersNewState {
  file: File | null
  fileName: string
  email: string
}

class UsersNew extends Component<UsersNewProps, UsersNewState> {
  static mapDispatchToProps = {
    addNewUsers
  }
  static mapStateToProps = ({ user }: { user: APIUserFull }) => ({
    organizationId: user.organization.id
  })

  state = {
    fileName: '',
    file: null,
    email: ''
  }

  handlerChangeEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault()
    const { value } = e.target
    const emailVal = !email(value) ? value : ''
    this.setState({ email: emailVal })
  }

  handlerDragFile = (files: File[]) => {
    this.setState({ file: files[0], fileName: files[0].name })
  }

  handlerClear = () => this.setState({ file: null, fileName: '' })

  handlerSubmit = () => {
    const { email, file } = this.state
    const formData = new FormData()
    const payload: Payload = {}
    if (file) {
      formData.append('file', file || '')
      payload.file = formData
    }
    if (email) {
      payload.email = {
        email,
        organizationId: this.props.organizationId
      }
    }
    this.props.addNewUsers(payload)
  }

  render() {
    const { file, email } = this.state
    const btnIsDisabled = !(file || email)

    return (
      <div className='page-container'>
        <HeaderPageContent title='Add user' linkURL={Routes.Users} linkTitle='Back to List Users' />

        <div className='page-content'>
          <div className='input-block'>
            <label htmlFor='new-user' className='label'>
              Add User Email
            </label>
            <Input
              className='--small'
              placeholder='User Email'
              id='new-user'
              onChange={this.handlerChangeEmail}
            />
            {/* <span>You can enter multiple emails separated by a comma or a space.</span> */}
          </div>

          <UploadFileContainer handler={this.handlerDragFile} fileName={this.state.fileName} />

          <div className='controls flex-right'>
            <div className='controls__item'>
              <Button className='btn-small --grey' onClick={this.handlerClear} children='Cancel' />
            </div>
            <div className='controls__item'>
              <Button
                className='btn-small'
                disabled={btnIsDisabled}
                onClick={this.handlerSubmit}
                children='Add users'
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  UsersNew.mapStateToProps,
  UsersNew.mapDispatchToProps
)(UsersNew)
