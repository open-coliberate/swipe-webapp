import React from 'react'
import { DragDropContextProvider } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import UploadFileCore from './UploadFileCore'

interface Type {
  handler: (a: any) => void
  fileName: string
}
const UploadFileContainer: React.StatelessComponent<Type> = props => (
  <div className='dnd-zone'>
    <div className='dnd-title'>Or you can dowload CSV files</div>
    <DragDropContextProvider backend={HTML5Backend}>
      <UploadFileCore handler={props.handler} fileName={props.fileName} />
    </DragDropContextProvider>
  </div>
)

export default UploadFileContainer
