import React, { ReactElement } from 'react'
import { DropTarget, DropTargetMonitor } from 'react-dnd'
import { NativeTypes } from 'react-dnd-html5-backend'
import classNames from 'classnames'

interface UploadFileCoreProps {
  connectDropTarget: (a: any) => ReactElement
  isOver: boolean
  handler: (a: any) => void
  fileName: string
}

interface Connect {
  dropTarget: () => any
}

const spec = {
  drop(props: { handler: (a: any) => void }, monitor: DropTargetMonitor) {
    const { files } = monitor.getItem()
    props.handler(files)
  }
}

const collect = (connect: Connect, monitor: DropTargetMonitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver()
})

const UploadFileCore: React.FunctionComponent<UploadFileCoreProps> = ({
  connectDropTarget,
  isOver,
  fileName
}) => {
  const msg = fileName
    ? `You uploaded file: ${fileName}`
    : 'CSV should contain emails of new users and optionally - names of new users'
  const cn = classNames('dropFileField', { hovered: isOver }, { 'file-selected': fileName })

  return connectDropTarget(
    <div className={cn}>
      <div className='--blue'>Drag and Drop your CSV file</div>
      <div>{msg}</div>
    </div>
  )
}

export default DropTarget(NativeTypes.FILE, spec, collect)(UploadFileCore)
