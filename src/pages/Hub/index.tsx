import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm, InjectedFormProps, reset } from 'redux-form'

import { createHub as onSubmit } from '../../actions'
import { HeaderPageContent } from '../../components/Headers'
import FormInput from '../../components/FormInput'
import Button from '../../components/Button'
import { Routes } from '../../utils/routes'
import { required } from '../../utils/helper'
import { RootState } from '../../reducers/state'
import { APIHub } from '../../models/APIModels'

interface HubProps extends InjectedFormProps {
  onSubmit: () => void
  name?: string
  match: {
    params: {
      id: string
    }
  }
  hub: APIHub[]
}
interface HubState {
  isEdit: boolean
}

class Hub extends Component<HubProps, HubState> {
  static mapDispatchStateToProps = {
    onSubmit
  }
  static mapStateToProps = ({ hubs }: RootState) => ({
    hub: hubs.data
  })

  state = {
    isEdit: false
  }

  componentDidMount() {
    const {
      initialize,
      hub,
      match: { params }
    } = this.props
    if (params) {
      const thisHub = hub.find(el => el.id === params.id)
      const initValues = thisHub
        ? {
            address: thisHub.address,
            title: thisHub.title,
            hubID: thisHub.id
          }
        : null
      initValues && initialize(initValues)
      initValues && this.setState({ isEdit: true })
    }
  }

  render() {
    const { pristine, reset, handleSubmit } = this.props
    const title = this.state.isEdit ? 'Edit' : 'Create'

    return (
      <div className='page-container'>
        <HeaderPageContent
          title={`${title} new HUB`}
          linkURL={Routes.Calendars}
          linkTitle='Back to Calendar'
        />

        <div className='page-content'>
          <form onSubmit={handleSubmit}>
            <div className='input-block'>
              <div className='form-row'>
                <Field
                  name='title'
                  type='text'
                  placeholder='Enter HUB&#39; name'
                  className='half'
                  label='HUB Name'
                  component={FormInput}
                  validate={required}
                />
                <Field
                  name='address'
                  type='text'
                  placeholder='Address'
                  className='half'
                  label='Hub address'
                  component={FormInput}
                  validate={required}
                />
                <Field name='hubID' type='hidden' component={FormInput} />
              </div>
            </div>
            <div className='controls flex-right'>
              <div className='controls__item'>
                <Button className='btn-small --grey' onClick={reset} children='Cancel' />
              </div>
              <div className='controls__item'>
                <Button
                  className='btn-small'
                  type='submit'
                  disabled={pristine}
                  children={`${title} Hub`}
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

const WithConnect = connect(
  Hub.mapStateToProps,
  Hub.mapDispatchStateToProps
)

const WithForm = reduxForm({
  form: 'createHub',
  onSubmitSuccess: (props, dispatch) => {
    !props.hubID && dispatch(reset('createHub'))
  }
})

export default compose(
  WithConnect,
  WithForm
)(Hub)
