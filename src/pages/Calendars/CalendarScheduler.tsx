import 'react-big-scheduler/lib/css/style.css'

import format from 'date-fns/format'
import addDays from 'date-fns/add_days'
import subDays from 'date-fns/sub_days'
import React, { Component, Suspense } from 'react'
import Scheduler, { CellUnits, DATE_FORMAT, SchedulerData, ViewTypes } from 'react-big-scheduler'
import get from 'lodash/get'
import classnames from 'classnames'

import Modal from '../Modal'
import {
  APISpace,
  APIBooking,
  APITool,
  APIEventBooking,
  APIEventBookingExt,
  APIWorkingHours,
  Roles
} from '../../models'
import { BookingDto } from '../../routines/bookings/bookingsPost'
import {
  transformBookingToEvent,
  valueSelectPrettier,
  toolsSelectFormatter,
  bookingToolPayloadFormat,
  bookingSpacePayloadFormat
} from '../../utils/helper'
import schedulerConfig from './configCalendarScheduler'
import { FormBookingValues, recurringOptions } from './CalendarModal'

const ModalEditEvent = React.lazy(() => import('./CalendarModal'))

export interface SchedulerEventModel {
  id: string
  start: string
  end: string
  resourceId: string
  title: string
  bgColor?: string
  movable: boolean
  startResizable: boolean
  endResizable: boolean
  rrule?: string
  type: 1 | 2 // 1 - toolsBooking, 2 - spaceBooking
}

export interface NewEventPropsModel {
  schedulerData: {
    events: { forEach: (arg0: (item: any) => void) => void }
    addEvent: (event: SchedulerEventModel) => void
  }
  slotId: string
  slotName: string
  start: string
  end: string
  type: any
  item: any
}

export interface BookingDtoExtended extends BookingDto {
  workingTime: number[]
  isPosting: boolean
}
interface State {
  viewModel: any
  visible: boolean
  editEventOptions: any | null
}
interface SchedulerResourceModel {
  id: string
  name: string
}

interface Props {
  currHubID: string
  spaces: APISpace[]
  bookings: (APIBooking | APIEventBookingExt)[]
  tools: (APITool | APISpace)[]
  myId: string
  selectedDate: string
  bookingForSave: BookingDtoExtended
  myRole: Roles
  postBooking: (bookingEvent: BookingDto) => void
  createBooking: (data: BookingDtoExtended) => void
  resetBooking: () => void
  handleDateChange: (date: string) => void
  bookingEventActions: (data: APIEventBooking) => void
}

const generateInitValues = (eventOptions: any, tools: (APITool | APISpace)[]) => {
  const initialValues = eventOptions
    ? {
        buddyEmail: '',
        title: '',
        recurring: recurringOptions[0],
        toolIds: [],
        from: valueSelectPrettier(Number(eventOptions.start.slice(11, 13))),
        to: valueSelectPrettier(Number(eventOptions.end.slice(11, 13)))
      }
    : undefined
  const toolsOptions = get(eventOptions, 'isSpace', false)
    ? toolsSelectFormatter({ spaceID: eventOptions.slotId, tools })
    : null
  return {
    initialValues,
    toolsOptions
  }
}

export default class CalendarScheduler extends Component<Props, State> {
  private resources: SchedulerResourceModel[] = [] // this should be tools
  private events: SchedulerEventModel[] = []
  private today = format(new Date(), DATE_FORMAT)
  private schedulerData: any
  private coordinates: { x: number; y: number }

  constructor(props: Props) {
    super(props)
    schedulerConfig.eventItemPopoverEnabled =
      props.myRole === Roles.LAB_MANAGER || props.myRole === Roles.SUPER_USER
    this.schedulerData = new SchedulerData(
      this.today,
      ViewTypes.Day,
      false,
      false,
      schedulerConfig,
      {
        isNonWorkingTimeFunc: (schedulerData: any, time: any) => {
          let isWorking = false
          const { localeMoment } = schedulerData
          const workingHours: APIWorkingHours[] = get(this.props, 'spaces[1].workingHours', [])
          if (schedulerData.cellUnit === CellUnits.Hour) {
            const hour = localeMoment(time).hour()
            const dayOfWeek = localeMoment(time).weekday()
            isWorking = workingHours.some((day: any) =>
              dayOfWeek === day.day && day.isWorking ? hour >= day.from && hour < day.to : false
            )
          } else {
            isWorking = workingHours.some(day => {
              const dayOfWeek = localeMoment(time).weekday()
              return dayOfWeek === day.day && day.isWorking === true
            })
          }

          return !isWorking
        }
      }
    )
    this.schedulerData.localeMoment.locale('en')
    this.schedulerData.setResources(this.resources)
    this.schedulerData.setEvents(this.events)
    this.coordinates = {
      x: 0,
      y: 0
    }
    this.state = {
      viewModel: this.schedulerData,
      visible: false,
      editEventOptions: null
    }
  }

  updateData(): void {
    this.events = transformBookingToEvent(this.props.bookings)
    this.resources = this.props.tools.map(val => ({
      id: val.id,
      name: val.title,
      className: 'hub' in val ? 'space-row' : ''
    }))
    this.schedulerData.setEvents(this.events)
    this.schedulerData.setResources(this.resources)
    this.setState({ viewModel: this.schedulerData })
  }

  componentDidUpdate(prevProps: Props) {
    const hasChanges =
      this.props.currHubID !== prevProps.currHubID ||
      this.props.bookings.length !== prevProps.bookings.length ||
      this.props.selectedDate !== prevProps.selectedDate

    hasChanges && this.updateData()
  }

  componentDidMount() {
    this.updateData()
  }

  prevClick = () => {
    const prevDay = format(subDays(new Date(this.props.selectedDate), 1), 'YYYY-MM-DD')
    this.schedulerData.prev()
    this.props.handleDateChange(prevDay)
  }

  nextClick = () => {
    const nextDay = format(addDays(new Date(this.props.selectedDate), 1), 'YYYY-MM-DD')
    this.schedulerData.next()
    this.props.handleDateChange(nextDay)
  }

  onSelectDate = (
    schedulerData: {
      setDate: (arg: any) => void
      setEvents: (arg: any) => void
      setResources: (arg: any) => void
    },
    date: any
  ) => {
    schedulerData.setDate(date)
    this.props.handleDateChange(date.format(DATE_FORMAT))
  }

  newEvent = (
    schedulerData: object,
    slotId: string,
    slotName: string,
    start: string,
    end: string
  ) => {
    const workSpace = this.props.spaces.find(({ id }) => id === slotId)
    const tool = !workSpace && this.props.tools.find(({ id }) => id === slotId)
    const editEventOptions = {
      eventType: workSpace ? 'eventBooking' : 'toolBooking',
      isSpace: Boolean(workSpace),
      schedulerData,
      spaceTitle: workSpace ? workSpace.title : get(tool, 'controller.space.title', ''),
      slotId,
      slotName,
      start,
      end
    }
    this.setState({ editEventOptions })
  }

  toolBooking = (formData: FormBookingValues) => {
    const { editEventOptions } = this.state
    const payload: BookingDto = bookingToolPayloadFormat({
      editEventOptions,
      ownerId: this.props.myId,
      formData
    })

    this.props.postBooking(payload)
    this.setState({ editEventOptions: null })
  }

  eventBooking = (formData: FormBookingValues) => {
    const { editEventOptions } = this.state
    const payload: APIEventBooking = bookingSpacePayloadFormat({ editEventOptions, formData })
    this.props.bookingEventActions(payload)

    this.setState({
      editEventOptions: null
    })
  }

  getCoordinates = (e: { pageX: number; pageY: number }): void => {
    if (!this.state.editEventOptions) {
      this.coordinates.x = e.pageX
      this.coordinates.y = e.pageY
    }
  }

  eventItemRender = (
    schedulerData: any,
    event: SchedulerEventModel,
    bgColor: any,
    isStart: any,
    isEnd: any,
    itemClass: string,
    minHeight: any
  ) => {
    const divStyle: React.CSSProperties = {
      backgroundColor: event.type === 1 ? '#89969f' : '#5b5b5b',
      minHeight
    }
    const itemCN = classnames(itemClass, { 'event-space': event.type === 2 })
    const textItemCN = `${itemClass}__text`
    return (
      <span key={event.id} className={itemCN} style={divStyle}>
        <span className={textItemCN}>{event.title}</span>
      </span>
    )
  }

  eventItemPopoverRender = ({  }: any, event: SchedulerEventModel) => (
    <div className='popover-content'>
      <a href='#' onClick={this.handleBookingDelete(event.id, event.type)}>
        Delete Event
      </a>
    </div>
  )

  handleBookingDelete = (id: string, bookingType: number) => (e: React.SyntheticEvent) => {
    e.preventDefault()
    console.log(id, bookingType)
  }

  handleCancel = (): void => this.setState({ editEventOptions: null })

  render() {
    const { tools, spaces } = this.props
    const { editEventOptions } = this.state
    const { initialValues, toolsOptions } = generateInitValues(editEventOptions, tools)

    return spaces.length ? (
      <>
        <div className='calendar-scheduler' onMouseMove={this.getCoordinates}>
          <h3 className='calendar-scheduler__title'>Manage lab calendar</h3>
          <Scheduler
            schedulerData={this.state.viewModel}
            prevClick={this.prevClick}
            nextClick={this.nextClick}
            onSelectDate={this.onSelectDate}
            onViewChange={() => false}
            newEvent={this.newEvent}
            eventItemTemplateResolver={this.eventItemRender}
            eventItemPopoverTemplateResolver={this.eventItemPopoverRender}
          />
        </div>
        {editEventOptions ? (
          <Modal
            arrow
            shouldCalculatePosition
            coordinates={this.coordinates}
            closeModal={this.handleCancel}
          >
            <Suspense fallback={<div />}>
              <ModalEditEvent
                space={editEventOptions.spaceTitle}
                slotName={editEventOptions.slotName}
                isSpace={editEventOptions.isSpace}
                toolsOptions={toolsOptions}
                initialValues={initialValues}
                onSubmit={this[editEventOptions.eventType] || null}
                closeModal={this.handleCancel}
              />
            </Suspense>
          </Modal>
        ) : null}
      </>
    ) : (
      <div>Sorry, this Hub not ready yet...</div>
    )
  }
}
