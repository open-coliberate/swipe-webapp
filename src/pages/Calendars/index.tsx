import './style.css'

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import get from 'lodash/get'

import {
  bookingsListRoutineActions,
  hubsListRoutineActions,
  postBookingRoutineActions,
  spacesListRoutineActions,
  toolsListRoutineActions,
  setCurrHub,
  createBooking,
  resetBooking,
  setBookingDate,
  bookingEventActions,
  getBookingEventList
} from '../../actions'
import Loader from '../../components/Loader'
import {
  APISpace,
  BookingsListExtended,
  HubsExtended,
  SpacesListExtended,
  ToolsListExtended,
  APIEventBooking,
  APIEventBookingExt,
  Roles
} from '../../models'
import { groupInstance } from '../../utils/helper'
import { RootState } from '../../reducers'
import withDragDropContext from './withDnDContext'
import { BookingDtoExtended } from './CalendarScheduler'

const Scheduler = React.lazy(() => import('./CalendarScheduler'))
const Nav = React.lazy(() => import('./CalendarNav'))

interface ComponentPropsFromState {
  bookings: BookingsListExtended
  tools: ToolsListExtended
  spaces: SpacesListExtended
  events: APIEventBookingExt[]
  hubs: HubsExtended
  myId: string
  myRole: Roles
  currBookingForSave: BookingDtoExtended
  currHub: string
  selectedDate: string
}

interface ComponentPropsFromDispatch {
  fetchBookings: () => void
  fetchTools: () => void
  fetchSpaces: () => void
  fetchHubs: () => void
  setHub: (id: string) => void
  postBooking: (bookingEvent: BookingDtoExtended) => void
  createBooking: (data: BookingDtoExtended) => void
  setBookingDate: (date: string) => void
  resetBooking: () => void
  getBookingEventList: () => void
  bookingEventActions: (data: APIEventBooking) => void
}

type Props = ComponentPropsFromDispatch & ComponentPropsFromState

class Calendars extends Component<Props> {
  static mapStateToProps = (state: RootState) => ({
    bookings: state.bookings.bookingsList,
    tools: state.tools.toolsList,
    hubs: state.hubs,
    spaces: state.spaces.spacesList,
    events: state.events.list,
    myId: state.user.id,
    myRole: state.user.role,
    currBookingForSave: state.bookings.bookingItem,
    currHub: state.hubs.currHub,
    selectedDate: state.bookings.selectedDate
  })

  static mapDispatchToProps = {
    fetchBookings: bookingsListRoutineActions,
    fetchTools: toolsListRoutineActions,
    fetchHubs: hubsListRoutineActions,
    fetchSpaces: spacesListRoutineActions,
    setHub: setCurrHub,
    postBooking: postBookingRoutineActions,
    createBooking,
    resetBooking,
    setBookingDate,
    bookingEventActions,
    getBookingEventList
  }

  componentDidMount() {
    this.props.fetchSpaces()
    this.props.fetchBookings()
    this.props.fetchTools()
    this.props.fetchHubs()
    this.props.getBookingEventList()
  }

  get isFetching(): boolean {
    if (this.props.spaces.isFetching) {
      return true
    }
    if (this.props.hubs.isFetching) {
      return true
    }
    if (this.props.tools.isFetching) {
      return true
    }
    if (this.props.currBookingForSave.isPosting) {
      return true
    }
    return false
  }

  get isAdmin(): boolean {
    return this.props.myRole === 'owner'
  }

  render() {
    if (this.isFetching) return <Loader />
    const { bookings: bookingsArr, tools: toolsArr, events: eventsArr } = this.props
    const spacesArr: APISpace[] = get(this.props, 'spaces.data', [])
    const spaces = spacesArr.filter(space => space.hub.id === this.props.currHub)
    const { bookings, tools } = groupInstance({ spaces, bookingsArr, toolsArr, eventsArr })

    return (
      <div className='calendar'>
        <>
          <React.Suspense fallback={<Loader />}>
            <Nav
              currHubId={this.props.currHub}
              hubs={this.props.hubs}
              spaces={this.props.spaces}
              role={this.props.myRole}
            />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <div className='calendar-container'>
              {this.isFetching ? (
                <Loader />
              ) : (
                <Scheduler
                  spaces={spaces}
                  bookings={bookings}
                  tools={tools}
                  currHubID={this.props.currHub}
                  myId={this.props.myId}
                  postBooking={this.props.postBooking}
                  createBooking={this.props.createBooking}
                  resetBooking={this.props.resetBooking}
                  bookingForSave={this.props.currBookingForSave}
                  handleDateChange={this.props.setBookingDate}
                  selectedDate={this.props.selectedDate}
                  bookingEventActions={this.props.bookingEventActions}
                  myRole={this.props.myRole}
                />
              )}
            </div>
          </React.Suspense>
        </>
      </div>
    )
  }
}

export default compose(
  connect(
    Calendars.mapStateToProps,
    Calendars.mapDispatchToProps
  ),
  withDragDropContext
)(Calendars)
