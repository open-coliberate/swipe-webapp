import React, { ReactElement } from 'react'

interface SubMenuProps {
  actions?: object
}
interface MenuList {
  title: string | ReactElement
  accessor: string | null
}

const menuList: MenuList[] = [
  { title: 'Delete this Hub', accessor: 'delete' },
  { title: 'Edit this Hub', accessor: 'redirectTo' }
]

const SubMenu: React.StatelessComponent<SubMenuProps> = ({ actions }) => (
  <>
    {menuList.map((el: MenuList, i: number) => (
      <div
        key={`${el.title}_${i}`}
        className='menu__item'
        onClick={(actions && actions[el.accessor ? el.accessor : '']) || undefined}
      >
        <span className='item__text'>{el.title}</span>
      </div>
    ))}
  </>
)

export default SubMenu
