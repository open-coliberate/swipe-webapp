import React, { Component } from 'react'
import { connect } from 'react-redux'
import { animated, config, Spring } from 'react-spring/renderprops'
import { push } from 'connected-react-router'
import classnames from 'classnames'

import { ServiceSpaces } from '.'
import { NavHub } from './CalendarNavHub'
import { NavLinks } from './CalendarNavLinks'
import SubMenu from './CalendarNavSubmenu'
import Modal from '../../Modal'
import { MoreDefault } from '../../../components/More'
import ArrowIcon from '../../../components/Icons/Arrow'
import Button from '../../../components/Button'
import { setCurrHub, deleteHub } from '../../../actions'
import { Routes } from '../../../utils/routes'
import { Roles } from '../../../models'

export interface CalendarNavParentProps {
  title: string
  spacesCount: number
  spaces: Array<ServiceSpaces>
  hubId: string
  currHub: string
  role: string
}

export interface CalendarNavState {
  isActive: boolean
  showModal: boolean
}

export interface ComponentPropsFromDispatch {
  setCurrHub: (id: string) => void
  deleteHub: (id: string) => void
  push: (arg: string) => void
}

type CalendarNavProps = CalendarNavParentProps & ComponentPropsFromDispatch

class CalendarNav extends Component<CalendarNavProps, CalendarNavState> {
  state = {
    isActive: this.props.currHub === this.props.hubId,
    showModal: false
  }

  static mapDispatchToProps = {
    setCurrHub,
    deleteHub,
    push
  }

  handleClick = (e: React.MouseEvent) => {
    e.stopPropagation()
    this.setState(({ isActive }) => ({ isActive: !isActive }))
    this.props.setCurrHub(this.props.hubId)
  }

  handlerShowModal = (e: React.SyntheticEvent) => {
    e.preventDefault()
    e.stopPropagation()
    this.setState({ showModal: true })
  }

  handlerDeleteHub = (e: React.SyntheticEvent) => {
    this.props.deleteHub(this.props.hubId)
    this.setState({ showModal: false })
  }

  handlerRedirectTo = (e: React.SyntheticEvent) => {
    e.preventDefault()
    e.stopPropagation()
    this.props.push(`${Routes.Hub}/${this.props.hubId}`)
  }

  render() {
    const { title, spaces, spacesCount, role } = this.props
    const actions = {
      delete: this.handlerShowModal,
      redirectTo: this.handlerRedirectTo
    }
    const { isActive } = this.state
    const hubPermission = role === Roles.LAB_MANAGER

    return (
      <>
        <div onClick={this.handleClick} className='calendar-nav__item'>
          <NavHub title={title} count={spacesCount} isActive={isActive}>
            {isActive && hubPermission ? (
              <MoreDefault className='--lite'>
                <SubMenu actions={actions} />
              </MoreDefault>
            ) : (
              <ArrowIcon />
            )}
          </NavHub>
          <Spring
            native
            config={config.default}
            from={{
              opacity: isActive ? 0 : 1,
              height: isActive ? 0 : 'auto'
            }}
            to={{
              opacity: isActive ? 1 : 0,
              height: isActive ? 'auto' : 0
            }}
          >
            {props => (
              <animated.div className={classnames({ disabled: isActive })} style={props}>
                <NavLinks spaces={spaces} role={role} />
              </animated.div>
            )}
          </Spring>
        </div>
        {this.state.showModal ? (
          <Modal closeModal={() => this.setState({ showModal: false })}>
            <div>
              Do you want remove this Hub?
              <div className='controls flex-right'>
                <div className='controls__item'>
                  <Button
                    className='--grey'
                    onClick={() => this.setState({ showModal: false })}
                    children='Cancel'
                  />
                </div>
                <div className='controls__item'>
                  <Button onClick={this.handlerDeleteHub} children='Delete' />
                </div>
              </div>
            </div>
          </Modal>
        ) : null}
      </>
    )
  }
}

export default connect(
  null,
  CalendarNav.mapDispatchToProps
)(CalendarNav)
