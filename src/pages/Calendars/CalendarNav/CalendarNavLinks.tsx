import * as React from 'react'
import { Link } from 'react-router-dom'

import { Roles } from '../../../models'
import AddNewIcon from '../../../components/Icons/AddNew'
import { Routes } from '../../../utils/routes'
import { CalendarLink } from './CalendarLink'
import { ServiceSpaces } from './'

export interface IAppProps {
  spaces: ServiceSpaces[]
  role: string
}

const itemCN = 'calendar-nav-item-el'

const AddBtn = () => (
  <Link to={Routes.NewSpace} className='add-space'>
    <AddNewIcon />
  </Link>
)

export function NavLinks({ spaces, role }: IAppProps) {
  const spacePermission = role === Roles.LAB_MANAGER || role === Roles.SUPER_USER

  return (
    <div className='calendar-nav-item'>
      <span className='calendar-nav-item-title'>
        Spaces
        {spacePermission ? <AddBtn /> : null}
      </span>
      {spaces.map((el, index) => {
        const key = el.id + index

        return spacePermission ? (
          <CalendarLink key={key} id={el.id} title={el.title} cn={itemCN} />
        ) : (
          <span key={key} className={itemCN}>
            {el.title}
          </span>
        )
      })}
    </div>
  )
}
