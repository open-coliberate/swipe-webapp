import * as React from 'react'
import { Link } from 'react-router-dom'

import { Routes } from '../../../utils/routes'

interface CalendarLinkProps {
  id: string
  title: string
  cn: string
}

export const CalendarLink: React.FunctionComponent<CalendarLinkProps> = props => (
  <Link className={props.cn} to={Routes.Space + props.id}>
    {props.title}
  </Link>
)
