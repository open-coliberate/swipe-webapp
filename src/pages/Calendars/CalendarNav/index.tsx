import * as React from 'react'
import { NavLink } from 'react-router-dom'

import { APIHub, HubsExtended, SpacesListExtended, Roles } from '../../../models'
import { Routes } from '../../../utils/routes'
import Ico from '../../../components/Ico'
import CalendarNav from './CalendarNav'

export interface CalendarNavContainerProps {
  spaces: SpacesListExtended
  hubs: HubsExtended
  currHubId: string
  role: string
}

export interface ServiceSpaces {
  id: string
  title: string
}

export interface HubsWithSpaces {
  id: string
  title: string
  spacesCount: number
  spaces: Array<ServiceSpaces>
}

export default class CalendarNavContainer extends React.Component<CalendarNavContainerProps> {
  public matchSpacesToHubs(hubs: HubsExtended, spaces: SpacesListExtended) {
    const output = hubs.data.reduce((prev: HubsWithSpaces[], curr: APIHub) => {
      let spacesCount = 0
      const matchedSpaces = spaces.data.filter(space => {
        if (space.hub.id === curr.id) {
          spacesCount++
          return true
        } else {
          return false
        }
      })
      const mutatedArr = matchedSpaces.map(el => ({
        id: el.id,
        title: el.title
      }))
      return [
        ...prev,
        {
          id: curr.id,
          title: curr.title,
          spacesCount,
          spaces: mutatedArr
        }
      ]
    }, [])

    return output
  }

  render() {
    const { role, hubs, spaces } = this.props
    const nav = this.matchSpacesToHubs(hubs, spaces) as HubsWithSpaces[]

    return (
      <div className='calendar-nav'>
        {nav.map(el => (
          <CalendarNav
            key={el.id}
            title={el.title}
            spacesCount={el.spacesCount}
            spaces={el.spaces}
            hubId={el.id}
            currHub={hubs.currHub}
            role={role}
          />
        ))}
        {role === Roles.LAB_MANAGER ? (
          <div className='calendar-nav__item link'>
            <div className='calendar-nav-header'>
              <NavLink to={Routes.Hub}>
                Create new HUB
                <Ico isVisible icoType='plus' icoPosition='after' />
              </NavLink>
            </div>
          </div>
        ) : null}
      </div>
    )
  }
}
