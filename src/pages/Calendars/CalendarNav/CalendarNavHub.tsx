import * as React from 'react'
import classnames from 'classnames'

interface NavHubProps {
  title: string
  count: number
  isActive: boolean
}

export const NavHub: React.FunctionComponent<NavHubProps> = props => {
  const cn = classnames('calendar-nav-header', { active: props.isActive })

  return (
    <div className={cn}>
      {props.children}
      <div className='calendar-nav-header-title'>{props.title}</div>
      <div className='calendar-nav-header-spaces'>{`${props.count}`} Spaces</div>
    </div>
  )
}
