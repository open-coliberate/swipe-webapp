import React from 'react'
import { reduxForm, Form, Field, InjectedFormProps } from 'redux-form'
import { Dispatch } from 'redux'
import classnames from 'classnames'

import Button from '../../components/Button'
import FormInput from '../../components/FormInput'
import FormSelect from '../../components/FormSelect'
import ToolSelect from '../../components/FormToolsSelect'
import { timeHourGenerator, TimeLabel } from '../../utils/helper'

export interface FormBookingValues {
  buddyEmail: string
  title: string
  recurring: { value: string }
  toolIds: string[]
  from: TimeLabel | null
  to: TimeLabel | null
}

interface ParentProps {
  space: string
  slotName: string
  isSpace: boolean
  toolsOptions: { value: string; label: string }[] | null
  closeModal: () => void
}
interface DispatchProps {
  onSubmit: (data: FormBookingValues, dispatch: Dispatch<any>, props: ParentProps) => void
}

type AllPropsBookingForm = ParentProps &
  DispatchProps &
  InjectedFormProps<FormBookingValues, ParentProps>

export const recurringOptions = [
  { value: 'one_time', label: 'One time' },
  { value: 'per_week', label: 'Once a week' },
  { value: 'per_month', label: 'Once a month' }
]

const timePickerOptions = timeHourGenerator()

const ModalEditEvent: React.StatelessComponent<AllPropsBookingForm> = ({
  space,
  slotName,
  isSpace,
  toolsOptions,
  handleSubmit,
  closeModal
}) => {
  const handleCancel = (e: React.SyntheticEvent): void => {
    e.preventDefault()
    closeModal()
  }
  const title = isSpace ? 'Booking Event' : 'Booking tool'

  return (
    <Form className='modal-add-event' onSubmit={handleSubmit}>
      <div className='modal-title'>{title}</div>
      <div className='modal-body'>
        <div className='modal-body__row'>
          <div className={classnames({ 'modal-body__cell': !isSpace })}>
            <span className='cell__label'>Space:</span>
            <span className='cell__value'>{space}</span>
          </div>
          {!isSpace ? (
            <div className='modal-body__cell'>
              <span className='cell__label'>Tool:</span>
              <span className='cell__value'>{slotName}</span>
            </div>
          ) : null}
        </div>
        <div className='modal-body__row'>
          <div className='modal-body__cell'>
            <span className='cell__label'>Time from</span>
            <Field
              className='select full-width'
              name='from'
              options={timePickerOptions}
              component={FormSelect}
            />
          </div>
          <div className='modal-body__cell'>
            <span className='cell__label'>Time until</span>
            <Field
              className='select full-width'
              name='to'
              options={timePickerOptions}
              component={FormSelect}
            />
          </div>
        </div>
        {isSpace ? (
          <>
            <div className='modal-body__row'>
              <span className='cell__label'>Event title</span>
              <Field
                className='select full-width'
                name='title'
                placeholder='Event title'
                options={recurringOptions}
                component={FormInput}
              />
            </div>
            <div className='modal-body__row'>
              <span className='cell__label'>Event can be recurring</span>
              <Field
                className='select full-width'
                name='recurring'
                options={recurringOptions}
                component={FormSelect}
              />
            </div>

            <Field name='toolIds' options={toolsOptions} component={ToolSelect} />
          </>
        ) : (
          <div className='modal-body__row'>
            <span className='cell__label'>Add Buddy</span>
            <Field
              className='full-width'
              name='buddyEmail'
              placeholder='Add email address your biddy'
              component={FormInput}
            />
          </div>
        )}
        <div className='controls flex-right'>
          <div className='controls__item'>
            <Button className='--grey' onClick={handleCancel} children='Cancel' />
          </div>
          <div className='controls__item'>
            <Button className='btn-small' type='submit' children='Apply' />
          </div>
        </div>
      </div>
    </Form>
  )
}

export default reduxForm<FormBookingValues, ParentProps>({
  form: 'booking'
})(ModalEditEvent)
