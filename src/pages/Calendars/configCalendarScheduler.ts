import { ViewTypes } from 'react-big-scheduler'

export default {
  schedulerWidth: '82%',
  minuteStep: 60,
  dayCellWidth: 65,
  nonWorkingTimeHeadBgColor: '#ebedf3',
  nonWorkingTimeBodyBgColor: '#ebedf3',
  eventItemHeight: 32,
  schedulerMaxHeight: '100%',
  eventItemPopoverEnabled: false,
  views: [
    {
      viewName: 'Day',
      viewType: ViewTypes.Day,
      showAgenda: false,
      isEventPerspective: false
    }
  ]
}
