import './style.css'

import React, { Component } from 'react'
import { connect } from 'react-redux'

import { toolsTemplatesListRoutineActions } from '../../../actions'
import { ToolsTemplatesListExtended } from '../../../models'
import { RootState } from '../../../reducers'
import Loader from '../../../components/Loader'
import ToolTemplateItem from './ToolTemplateItem'

interface ComponentPropsFromDispatch {
  fetchTemplates: () => void
}

type ComponentProps = ComponentPropsFromDispatch & ToolsTemplatesListExtended

class ToolTemplateList extends Component<ComponentProps> {
  static mapStateToProps({ tools: { toolsTemplatesList } }: RootState): ToolsTemplatesListExtended {
    return toolsTemplatesList
  }

  static mapDispatchToProps = {
    fetchTemplates: toolsTemplatesListRoutineActions
  }

  componentDidMount() {
    this.props.fetchTemplates()
  }

  render() {
    if (this.props.isFetching) {
      return <Loader />
    }
    return this.props.isFetching ? (
      <Loader />
    ) : (
      <>
        {this.props.data.map(el => (
          <ToolTemplateItem key={el.id} data={el} />
        ))}
      </>
    )
  }
}

export default connect(
  ToolTemplateList.mapStateToProps,
  ToolTemplateList.mapDispatchToProps
)(ToolTemplateList)
