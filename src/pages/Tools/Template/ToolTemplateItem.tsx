import './style.css'

import React from 'react'

import { APITemplate } from '../../../models'
import { booleanToReadable } from '../../../utils/helper'
import Setting from './Setting/Setting'

export interface ToolItemProps {
  data: APITemplate
}

export default class ToolTemplateItem extends React.PureComponent<ToolItemProps> {
  private get getWorkingTime(): string {
    const { maxWorkTime, minWorkTime } = this.props.data
    if (maxWorkTime && minWorkTime) {
      return `${minWorkTime}h - ${maxWorkTime}h`
    } else {
      return 'no working hours set'
    }
  }

  render() {
    const {
      id,
      title,
      description,
      buddyMode,
      availableInWorkingHours,
      requiresTraining,
      sound
      // tools
    } = this.props.data

    return (
      <div className='tool-template'>
        <div className='tool-template-setting'>
          <Setting id={id} />
        </div>
        <div className='tool-template-header'>
          <span className='tool-template-header-title'>{title}</span>
          <div className='tool-template-header-time'>
            <span className='tool-template-header-time-title'>Time of work:</span>
            <span className='tool-template-header-time-hours'>{this.getWorkingTime}</span>
          </div>
        </div>
        <div className='tool-template-body'>
          <p className='tool-template-body-desc'>{description}</p>
          <div className='tool-template-body-opt'>
            <div className='tool-template-body-opt-item'>
              <span className='tool-template-body-opt-item-title'>Available in Buddy Mode</span>
              <span className='tool-template-body-opt-item-value'>
                {booleanToReadable(buddyMode)}
              </span>
            </div>
            <div className='tool-template-body-opt-item'>
              <span className='tool-template-body-opt-item-title'>Available in working hours</span>
              <span className='tool-template-body-opt-item-value'>
                {booleanToReadable(availableInWorkingHours)}
              </span>
            </div>
            <div className='tool-template-body-opt-item'>
              <span className='tool-template-body-opt-item-title'>Requires training </span>
              <span className='tool-template-body-opt-item-value'>
                {booleanToReadable(requiresTraining)}
              </span>
            </div>
            <div className='tool-template-body-opt-item'>
              <span className='tool-template-body-opt-item-title'>Sound</span>
              <span className='tool-template-body-opt-item-value'>{booleanToReadable(sound)}</span>
            </div>
          </div>
        </div>
        <div className='tool-template-devices'>
          <div className='tool-template-devices-title'>Device IDs list</div>
          <div className='tool-template-devices-container'>
            {/* {tools.map((item, index) => (
              <div key={index} className='tool-template-devices-item'>
                {`id number ${item}`}
              </div>
            ))} */}
          </div>
        </div>
      </div>
    )
  }
}
