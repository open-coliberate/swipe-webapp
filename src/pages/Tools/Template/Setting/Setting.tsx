import './style.css'

import React, { useState } from 'react'
import { animated, config, Spring } from 'react-spring/renderprops'

import SettingIcon from '../../../../components/Icons/Setting'

type Props = {
  id: string
}

const Setting = (props: Props) => {
  const [isOpened, toggleDropdown] = useState<boolean>(false)
  const toggle = () => toggleDropdown(isOpened => !isOpened)

  return (
    <div className='setting' onClick={toggle}>
      <SettingIcon />
      <Spring
        force
        native
        immediate={!isOpened}
        config={config.gentle}
        from={{
          opacity: isOpened ? 0 : 1,
          height: isOpened ? 0 : 'auto',
          poinerEvents: isOpened ? 'none' : 'auto'
        }}
        to={{
          opacity: isOpened ? 1 : 0,
          height: isOpened ? 'auto' : 0,
          poinerEvents: isOpened ? 'none' : 'auto'
        }}
      >
        {props => (
          <animated.div className='setting-menu' style={props}>
            <span className='setting-menu-item'>Edit template</span>
            <span className='setting-menu-item'>Delete template</span>
          </animated.div>
        )}
      </Spring>
    </div>
  )
}

export default Setting
