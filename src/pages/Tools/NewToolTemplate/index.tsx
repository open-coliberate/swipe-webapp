import './style.css'

import * as React from 'react'
import { RouteComponentProps } from 'react-router'

import ToolActions from './NewToolActions'
import ToolDevices from './NewToolDevices'
import ToolForm from './NewToolForm'
import ToolModes from './NewToolModes'
import { Routes } from '../../../utils/routes'
import { HeaderPageContent } from '../../../components/Headers'

type NewToolTemplateProps = RouteComponentProps<void>

export default class NewToolTemplate extends React.PureComponent<NewToolTemplateProps> {
  render() {
    return (
      <div className='page-container'>
        <HeaderPageContent
          title='Create Tool Template'
          linkURL={Routes.Tools}
          linkTitle='Back to Tools'
        />
        <div className='page-content'>
          <div className='newtemplate'>
            <ToolForm />
            <ToolModes />
            <ToolDevices />
            <ToolActions />
          </div>
        </div>
      </div>
    )
  }
}
