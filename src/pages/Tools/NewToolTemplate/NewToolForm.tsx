import * as React from 'react'
import { Field, InjectedFormProps, reduxForm } from 'redux-form'

import FormInput from '../../../components/FormInput'
import { FormTextarea } from '../../../components/FormTextarea'
import { Payload } from '../../../routines/tools/toolsTemplateNew'
import { required } from '../../../utils/helper'
import { submitTool } from './submit'

interface ToolForm extends InjectedFormProps<Payload> {}

class NewToolForm extends React.Component<ToolForm> {
  componentDidMount() {
    this.props.change('min', '')
    this.props.change('max', '')
    this.props.change('description', '')
  }
  render() {
    const { handleSubmit } = this.props
    return (
      <form className='newtemplate-form container' onSubmit={handleSubmit}>
        <div className='form-row'>
          <div className='half'>
            <Field
              name='title'
              type='text'
              placeholder='Enter tools name'
              className='newtemplate-form-item-input'
              label='Template Name'
              component={FormInput}
              validate={required}
            />
          </div>
          <div className='half'>
            <div className='form-row'>
              <span className='heading'>Time of work</span>
              <div className='form-row'>
                <Field
                  name='minWorkTime'
                  type='text'
                  placeholder='Minimal time'
                  className='half'
                  component={FormInput}
                />
                <Field
                  name='maxWorkTime'
                  type='text'
                  placeholder='Maximal time'
                  className='half'
                  component={FormInput}
                />
              </div>
            </div>
          </div>
        </div>

        <div className='form-row'>
          <Field
            name='description'
            type='text'
            placeholder='Text description tool'
            label='Description'
            component={FormTextarea}
          />
        </div>
      </form>
    )
  }
}

export default reduxForm<Payload>({
  form: 'new-tool',
  onSubmit: submitTool
})(NewToolForm)
