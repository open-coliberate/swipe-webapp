import * as React from 'react'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import { AnyAction, Dispatch } from 'redux'
import { Field, InjectedFormProps, reduxForm } from 'redux-form'

import { toolsListRoutineActions } from '../../../actions'
import FormSelect from '../../../components/FormSelect'
import { APITool } from '../../../models'
import { RootState } from '../../../reducers'
import { Payload } from '../../../routines/tools/toolsTemplateNew'
import { submitTool } from './submit'

interface ComponentsPropsFromDispatch {
  fetchTools: () => void
}

interface ComponentPropsFromState {
  data: APITool[]
  isFetching: boolean
}

type ToolDevicesProps = InjectedFormProps<Payload> &
  ComponentsPropsFromDispatch &
  ComponentPropsFromState

class ToolDevices extends React.Component<ToolDevicesProps> {
  static mapStateToProps(state: RootState): ComponentPropsFromState {
    return {
      data: state.tools.toolsList.data,
      isFetching: state.tools.toolsList.isFetching
    }
  }

  static mapDispatchToProps(dispatch: Dispatch<AnyAction>): ComponentsPropsFromDispatch {
    return {
      fetchTools: () => dispatch(toolsListRoutineActions())
    }
  }

  componentDidMount() {
    this.props.fetchTools()
  }

  render() {
    const options = this.props.data.map(el => ({
      value: el.id,
      label: el.title
    }))
    return (
      <div className='container'>
        <Field
          name='tools'
          className='newtemplate-form-input newtemplate-form-input__multiselect'
          options={options}
          isMulti
          isClearable
          component={FormSelect}
        />
      </div>
    )
  }
}

export default compose(
  reduxForm<Payload>({
    form: 'new-tool',
    onSubmit: submitTool
  }),
  connect(
    ToolDevices.mapStateToProps,
    ToolDevices.mapDispatchToProps
  )
)(ToolDevices)
