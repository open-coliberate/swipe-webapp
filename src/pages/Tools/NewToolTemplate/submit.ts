import { bindRoutineToReduxForm } from 'redux-saga-routines'

import { toolsTemplatesNewRoutineActions } from '../../../actions'

export const submitTool = bindRoutineToReduxForm(toolsTemplatesNewRoutineActions)
