import * as React from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router'
import { compose } from 'recompose'
import { InjectedFormProps, reduxForm } from 'redux-form'

import Button from '../../../components/Button'
import { RootState } from '../../../reducers'
import { Payload } from '../../../routines/tools/toolsTemplateNew'
import { Routes } from '../../../utils/routes'
import { submitTool } from './submit'

interface ComponentPropsFromState {
  organizationId: string
}

interface ToolsActionsState {
  shouldRedirect: boolean
  shouldReset: boolean
}

type ToolActionsProps = InjectedFormProps<Payload> &
  RouteComponentProps<void> &
  ComponentPropsFromState

class ToolActions extends React.Component<ToolActionsProps, ToolsActionsState> {
  constructor(props: ToolActionsProps) {
    super(props)
    this.state = {
      shouldRedirect: false,
      shouldReset: false
    }
  }

  static mapStateToProps(state: RootState): ComponentPropsFromState {
    return {
      organizationId: state.user.organization.id
    }
  }

  private handleCreate = () => {
    this.setState({
      shouldRedirect: true
    })
  }

  private handleSaveAndNew = () => {
    this.setState({
      shouldReset: true
    })
  }

  private handleClose = () => {
    this.props.history.goBack()
  }

  componentDidMount() {
    this.props.change('organizationId', this.props.organizationId)
  }

  componentDidUpdate() {
    if (this.props.submitSucceeded && this.state.shouldRedirect) {
      this.props.history.replace(Routes.Tools)
    }

    if (this.props.submitSucceeded && this.state.shouldReset) {
      this.props.reset()
    }
  }

  render() {
    const { handleSubmit } = this.props
    return (
      <form className='newtemplate-actions' onSubmit={handleSubmit}>
        <Button
          disabled={this.props.submitting}
          className='--grey'
          onClick={this.handleClose}
          children='Cancel'
        />
        <Button
          disabled={this.props.submitting || this.props.invalid}
          type='submit'
          onClick={this.handleCreate}
          children='Create template'
        />
        <Button
          disabled={this.props.submitting || this.props.invalid}
          className='--darkgrey'
          type='submit'
          icoPosition='after'
          icoType='plus'
          onClick={this.handleSaveAndNew}
          children='Save and add another template'
        />
      </form>
    )
  }
}

export default compose(
  reduxForm<Payload>({
    form: 'new-tool',
    onSubmit: submitTool
  }),
  connect(ToolActions.mapStateToProps),
  withRouter
)(ToolActions)
