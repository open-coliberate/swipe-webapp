import * as React from 'react'
import { Field, InjectedFormProps, reduxForm } from 'redux-form'

import { FormToggle } from '../../../components/FormToggle'
import { Payload } from '../../../routines/tools/toolsTemplateNew'

interface ToolModesProps extends InjectedFormProps<Payload> {}

class ToolModes extends React.Component<ToolModesProps> {
  componentDidMount() {
    this.props.change('buddyMode', false)
    this.props.change('availableInWorkingHours', false)
    this.props.change('requiresTraining', false)
    this.props.change('sound', false)
  }

  render() {
    const { handleSubmit } = this.props
    return (
      <form className='newtemplate-modes container' onSubmit={handleSubmit}>
        <div className='newtemplate-modes-item'>
          <div className='newtemplate-modes-item-title'>Available in Buddy Mode</div>
          <Field
            name='buddyMode'
            type='checkbox'
            className='newtemplate-form-input form-field'
            component={FormToggle}
          />
        </div>
        <div className='newtemplate-modes-item'>
          <div className='newtemplate-modes-item-title'>Available in working hours</div>
          <Field
            name='availableInWorkingHours'
            type='checkbox'
            className='newtemplate-form-input form-field'
            component={FormToggle}
          />
        </div>
        <div className='newtemplate-modes-item'>
          <div className='newtemplate-modes-item-title'>Requires training</div>
          <Field
            name='requiresTraining'
            type='checkbox'
            className='newtemplate-form-input form-field'
            component={FormToggle}
          />
        </div>
        <div className='newtemplate-modes-item'>
          <div className='newtemplate-modes-item-title'>Sound</div>
          <Field
            name='sound'
            type='checkbox'
            className='newtemplate-form-input form-field'
            component={FormToggle}
          />
        </div>
      </form>
    )
  }
}

export default reduxForm<Payload>({
  form: 'new-tool'
})(ToolModes)
