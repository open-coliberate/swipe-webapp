import './style.css'

import * as React from 'react'
import { connect } from 'react-redux'
import { AnyAction, compose, Dispatch } from 'redux'

import { toolsListRoutineActions } from '../../../actions'
import { ToolsListExtended } from '../../../models'
import { RootState } from '../../../reducers'
import ToolItem from './ToolItem'
import Loader from '../../../components/Loader'

interface ComponentPropsFromDispatch {
  fetchTools: () => void
}

type ComponentProps = ComponentPropsFromDispatch & ToolsListExtended

class ToolList extends React.PureComponent<ComponentProps> {
  static mapStateToProps(state: RootState): ToolsListExtended {
    return {
      data: state.tools.toolsList.data,
      isFetching: state.tools.toolsList.isFetching
    }
  }

  static mapDispatchToProps(dispatch: Dispatch<AnyAction>): ComponentPropsFromDispatch {
    return {
      fetchTools: () => dispatch(toolsListRoutineActions())
    }
  }

  componentDidMount() {
    this.props.fetchTools()
  }

  render() {
    if (this.props.isFetching) {
      return <Loader />
    }
    return (
      <React.Fragment>
        {this.props.data.map(el => (
          <ToolItem key={el.id} data={el} />
        ))}
      </React.Fragment>
    )
  }
}

export default compose(
  connect(
    ToolList.mapStateToProps,
    ToolList.mapDispatchToProps
  )
)(ToolList)
