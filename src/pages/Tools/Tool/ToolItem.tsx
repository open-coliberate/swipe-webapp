import './style.css'

import * as React from 'react'
import { APITool } from '../../../models'
import { booleanToReadable } from '../../../utils/helper'

export interface ToolItemProps {
  data: APITool
}

export default class ToolItem extends React.PureComponent<ToolItemProps> {
  private get getWorkingTime(): string {
    const { maxWorkTime, minWorkTime } = this.props.data.template
    if (maxWorkTime && minWorkTime) {
      return `${minWorkTime}h - ${maxWorkTime}h`
    } else {
      return 'no working hours set'
    }
  }

  render() {
    const { title, template } = this.props.data
    if (template) {
      return (
        <div className='tool-item'>
          <div className='tool-item-header'>
            <div className='tool-item-header-title'>{title}</div>
            <div className='tool-item-header-time'>
              <div className='tool-item-header-time-text'>Time of work</div>
              <div className='tool-item-header-time-values'>{this.getWorkingTime}</div>
            </div>
          </div>
          <table className='tool-item-table'>
            <thead>
              <tr>
                <th>device id</th>
                <th>current template name</th>
                <th>space name</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{template.id}</td>
                <td>{template.title}</td>
                <td>*spacenamefromapi*</td>
              </tr>
            </tbody>
          </table>
          <p className='tool-item-info'>{template.description}</p>
          <div className='tool-item-setting'>
            <span className='tool-item-setting-title'>Available in Buddy Mode</span>
            <span className='tool-item-setting-title'>{booleanToReadable(template.buddyMode)}</span>
          </div>
          <div className='tool-item-setting'>
            <span className='tool-item-setting-title'>Available in working hours</span>
            <span className='tool-item-setting-title'>
              {booleanToReadable(template.availableInWorkingHours)}
            </span>
          </div>
          <div className='tool-item-setting'>
            <span className='tool-item-setting-title'>Requires training</span>
            <span className='tool-item-setting-title'>
              {booleanToReadable(template.requiresTraining)}
            </span>
          </div>
          <div className='tool-item-setting'>
            <span className='tool-item-setting-title'>Sound</span>
            <span className='tool-item-setting-title'>{booleanToReadable(template.sound)}</span>
          </div>
          <button className='tool-item-button'>History of usage</button>
        </div>
      )
    } else {
      return <h1>no template set. please look at db</h1>
    }
  }
}
