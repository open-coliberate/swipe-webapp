import './style.css'

import * as React from 'react'
import { Link } from 'react-router-dom'
import { Routes } from '../../../utils/routes'

interface ToolsSwitchProps {
  currPath: string
}

export default class ToolsSwitch extends React.PureComponent<ToolsSwitchProps> {
  private getCurrClass(pathname: string): string {
    return `switch-item ${this.props.currPath === pathname ? 'switch-item__active' : ''}`
  }

  render() {
    return (
      <div className='switch'>
        <Link to={Routes.Tools} className={this.getCurrClass(Routes.Tools)}>
          Tools
        </Link>
        <Link to={Routes.Templates} className={this.getCurrClass(Routes.Templates)}>
          Templates
        </Link>
      </div>
    )
  }
}
