import './style.css'

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { Link, Route, Switch } from 'react-router-dom'

import { toolsListRoutineActions } from '../../actions'
import { ToolsListExtended, Roles } from '../../models'
import { RootState } from '../../reducers'
import { Routes } from '../../utils/routes'
import NewToolTemplate from './NewToolTemplate'
import ToolsSwitch from './Switch'
import ToolTemplateList from './Template/ToolTemplatesList'
import ToolList from './Tool/ToolList'

interface ToolsPropsFromDispatch {
  fetchTools: () => void
}

interface ToolsPropsFromState extends ToolsListExtended {
  role: string
}
interface CRUDPanelProps {
  url: string
}

type Props = ToolsPropsFromDispatch & ToolsPropsFromState & RouteComponentProps<void>

const ToolsCRUDPanel: React.StatelessComponent<CRUDPanelProps> = ({ url }) => (
  <div className='tool-panel'>
    <ToolsSwitch currPath={url} />
    <Link to={Routes.NewToolTemplate} className='tool-panel-btn'>
      Create tool template
    </Link>
  </div>
)
class Tools extends Component<Props> {
  static mapStateToProps({ tools, user }: RootState): ToolsPropsFromState {
    return {
      role: user.role,
      data: tools.toolsList.data,
      isFetching: tools.toolsList.isFetching
    }
  }

  static mapDispatchToProps = {
    fetchTools: toolsListRoutineActions
  }

  render() {
    const { role, location } = this.props
    const havePermission =
      role === Roles.LAB_MANAGER || role === Roles.SUPER_USER || role === Roles.OWNER
    return (
      <Switch>
        <Route exact path={Routes.NewToolTemplate} component={NewToolTemplate} />
        <Route>
          <div className='tool'>
            <div className='tool-container'>
              {havePermission ? <ToolsCRUDPanel url={location.pathname} /> : null}
              <Route exact path={Routes.Tools} component={ToolList} />
              <Route exact path={Routes.Templates} component={ToolTemplateList} />
            </div>
          </div>
        </Route>
      </Switch>
    )
  }
}

export default connect(
  Tools.mapStateToProps,
  Tools.mapDispatchToProps
)(Tools)
