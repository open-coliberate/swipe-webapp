import * as React from 'react'
import { Link } from 'react-router-dom'

import { Routes } from '../../utils/routes'

interface LoginProps {
  hasError: boolean
}

export class LoginTitle extends React.PureComponent<LoginProps> {
  render() {
    const { hasError } = this.props
    if (hasError) {
      return (
        <div className='login-message'>
          <span className='login-message-title login-message-title-error'>Error !</span>
          <p className='login-message-desc'>
            There is no such user in the system, if you want to register - come to the lab. <br />
            Or check the entered address
          </p>
        </div>
      )
    } else {
      return (
        <Link to={Routes.Home}>
          <h1 className='login-title'>Coliberate</h1>
        </Link>
      )
    }
  }
}
