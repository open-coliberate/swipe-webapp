import './style.css'

import * as React from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import { AnyAction, compose, Dispatch } from 'redux'
import { Field, InjectedFormProps, reduxForm } from 'redux-form'
import { bindRoutineToReduxForm } from 'redux-saga-routines'

import { loginRoutineActions, resetActivation, setActivation, setCookies } from '../../actions'
import Button from '../../components/Button'
import FormInput from '../../components/FormInput'
import Loader from '../../components/Loader'
import { UserFullExtended } from '../../models'
import { RootState } from '../../reducers'
import { Payload } from '../../routines/user/userLogin'
import { email, required } from '../../utils/helper'
import { Routes } from '../../utils/routes'
import { LoginTitle } from './LoginTitle'

interface ComponentsPropsFromDispatch {
  setToActivate: () => void
  resetToActivate: () => void
  setCookie: () => void
}

type LoginProps = InjectedFormProps<Payload> & UserFullExtended & ComponentsPropsFromDispatch

class Login extends React.PureComponent<LoginProps> {
  submitLogin = bindRoutineToReduxForm(loginRoutineActions)

  componentDidMount() {
    this.props.reset()
    this.props.resetToActivate()
  }

  public static mapStateToProps({ user }: RootState): UserFullExtended {
    return user
  }

  public static mapDispatchToProps(dispatch: Dispatch<AnyAction>): ComponentsPropsFromDispatch {
    return {
      setToActivate: () => dispatch(setActivation.perform()),
      resetToActivate: () => dispatch(resetActivation.perform()),
      setCookie: () => dispatch(setCookies.perform())
    }
  }

  render() {
    const { handleSubmit, isToSetNewPass, isUserAuth } = this.props
    if (isUserAuth) {
      return <Redirect to={Routes.Home} />
    } else {
      return (
        <div className='login-container'>
          {this.props.submitting && <Loader />}
          <LoginTitle hasError={isToSetNewPass} />
          <form className='login-form' onSubmit={handleSubmit(this.submitLogin)}>
            <Field
              name='email'
              placeholder='Email adderss'
              className='login-form-input'
              component={FormInput}
              validate={[email, required]}
            />
            <Field
              name='password'
              type='password'
              placeholder='Password'
              className='login-form-input'
              component={FormInput}
              validate={required}
            />
            <Button
              disabled={this.props.submitting || this.props.invalid}
              className='login-form-btn'
              type='submit'
              children='Sign in'
            />
            <Link className='login-form-forgot link-yellow' to='/password-recovery'>
              Forgot password?
            </Link>
          </form>
        </div>
      )
    }
  }
}

export default compose(
  connect(
    Login.mapStateToProps,
    Login.mapDispatchToProps
  ),
  reduxForm<Payload>({
    form: 'login'
  })
)(Login)
