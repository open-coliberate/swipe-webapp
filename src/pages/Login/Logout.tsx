import './style.css'

import * as React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { AnyAction, Dispatch } from 'redux'
import { bindRoutineToReduxForm } from 'redux-saga-routines'

import { loginRoutineActions, setActivation } from '../../actions'
import { Routes } from '../../utils/routes'

interface ComponentsPropsFromDispatch {
  setToActivate: () => void
}

type LoginProps = ComponentsPropsFromDispatch

class Logout extends React.PureComponent<LoginProps> {
  submitLogin = bindRoutineToReduxForm(loginRoutineActions)

  componentDidMount() {
    this.props.setToActivate()
  }

  public static mapDispatchToProps(dispatch: Dispatch<AnyAction>): ComponentsPropsFromDispatch {
    return {
      setToActivate: () => dispatch(setActivation.perform())
    }
  }
  public static mapStateToProps(): {} {
    return {}
  }

  render() {
    return <Redirect to={Routes.Home} />
  }
}

export default connect(
  Logout.mapStateToProps,
  Logout.mapDispatchToProps
)(Logout)
