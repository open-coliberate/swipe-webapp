import * as React from 'react'

export default class NotFound extends React.PureComponent {
  render() {
    return (
      <div className='main-container'>
        {' '}
        Looks like you've found a dead link, so... 404 my friend{' '}
      </div>
    )
  }
}
