import './style.css'

import * as React from 'react'
import { connect } from 'react-redux'
import { AnyAction, compose, Dispatch } from 'redux'

import { RootState } from '../../reducers'

interface ComponentPropsFromDispatch {
  performOpenSideMenu: () => void
  performCloseSideMenu: () => void
}

interface ComponentPropsFromState {
  isSideMenuOpened: boolean
}

type ComponentProps = ComponentPropsFromDispatch & ComponentPropsFromState

class Admins extends React.PureComponent<ComponentProps> {
  static mapStateToProps(state: RootState): ComponentPropsFromState | {} {
    return {}
  }

  static mapDispatchToProps(dispatch: Dispatch<AnyAction>): ComponentPropsFromDispatch | {} {
    return {}
  }

  render() {
    return <div className='admin'>admins</div>
  }
}

export default compose(
  connect(
    Admins.mapStateToProps,
    Admins.mapDispatchToProps
  )
)(Admins)
