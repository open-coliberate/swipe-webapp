import React from 'react'
import { Redirect, Route, Switch } from 'react-router'
import Home from './Home'
import Login from './Login'
import Logout from './Login/Logout'
import NotFound from './NotFound'
import PassRecovery from './PassRecovery'
import SetPass from './SetPass'
import { Routes } from '../utils/routes'
import './style.css'

type Props = {
  isAuth: boolean
}

class Root extends React.Component<Props> {
  handleRoutes(): any {
    if (this.props.isAuth) {
      return <Redirect to='/app' />
    } else {
      return <Redirect to='/login' />
    }
  }

  render() {
    return (
      <Switch>
        <Route exact path={Routes.Root} render={() => this.handleRoutes} />
        <Route path={Routes.Login} component={Login} />
        <Route path={Routes.NewPass} component={SetPass} />
        <Route path={Routes.PassRecovery} component={PassRecovery} />
        <Route path={Routes.Logout} component={Logout} />
        <Route path={Routes.Home} component={Home} />
        <Route component={NotFound} />
      </Switch>
    )
  }
}

export default Root
