import './style.css'

import * as React from 'react'
import { connect } from 'react-redux'
import { AnyAction, compose, Dispatch } from 'redux'
import { RootState } from '../../reducers'

interface ComponentPropsFromDispatch {}

interface ComponentPropsFromState {}

type ComponentProps = ComponentPropsFromDispatch & ComponentPropsFromState

class Analytics extends React.PureComponent<ComponentProps> {
  static mapStateToProps(state: RootState): ComponentPropsFromState | {} {
    return {}
  }

  static mapDispatchToProps(dispatch: Dispatch<AnyAction>): ComponentPropsFromDispatch | {} {
    return {}
  }

  render() {
    return <div className='analytics'>analytics</div>
  }
}

export default compose(
  connect(
    Analytics.mapStateToProps,
    Analytics.mapDispatchToProps
  )
)(Analytics)
