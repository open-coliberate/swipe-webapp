import '../Login/style.css'

import Cookies from 'js-cookie'
import * as React from 'react'
import { Redirect, RouteComponentProps } from 'react-router-dom'
import { Field, InjectedFormProps, reduxForm } from 'redux-form'
import { bindRoutineToReduxForm } from 'redux-saga-routines'

import { newPassRoutineActions } from '../../actions'
import Button from '../../components/Button'
import FormInput from '../../components/FormInput'
import { Payload } from '../../routines/user/userLogin'
import { required } from '../../utils/helper'
import { Routes } from '../../utils/routes'

interface SetPassRouteProps {
  id: string
}

export type SetPassProps = InjectedFormProps<Payload> & RouteComponentProps<SetPassRouteProps>

class SetPass extends React.PureComponent<SetPassProps> {
  submitForm = bindRoutineToReduxForm(newPassRoutineActions)

  componentDidMount() {
    this.props.reset()
  }

  render() {
    const hasCookie = !!Cookies.get('token')
    const { handleSubmit } = this.props
    if (hasCookie) {
      return <Redirect to={Routes.Home} />
    } else {
      return (
        <div className='login-container'>
          <div className='login-message'>
            <span className='login-message-title'>Hello 👋</span>
            <p className='login-message-desc'>
              You avtorizated your email address, you need to create your password
            </p>
          </div>
          <form className='login-form' onSubmit={handleSubmit(this.submitForm)}>
            <Field
              name='password'
              type='password'
              placeholder='Password'
              className='login-form-input'
              component={FormInput}
              validate={required}
            />
            <Field
              name='passwordConfirmation'
              type='password'
              placeholder='Confirm password'
              className='login-form-input'
              component={FormInput}
              validate={required}
            />
            <Button
              disabled={this.props.submitting || this.props.invalid}
              className='login-form-btn'
              type='submit'
              children='Create password'
            />
          </form>
        </div>
      )
    }
  }
}

export default reduxForm<Payload>({
  form: 'new-pass'
})(SetPass)
