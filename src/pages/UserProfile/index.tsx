import React, { Component } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import get from 'lodash/get'

import Loader from '../../components/Loader'
import ProfileForm from '../../components/ProfileForm'
import { HeaderPageContent } from '../../components/Headers'
import { Routes } from '../../utils/routes'
import { getInitValueForm } from '../../utils/helper'
import { getUserProfile, updateInfoRoutineActions as onSubmit } from '../../actions'
import { RootState } from '../../reducers'
import { State as ProfileReducer } from '../../reducers/profile'

type MatchParams = {
  id: string
}

interface ParentProps extends RouteComponentProps<MatchParams> {
  accountID: string
  role: string
}

interface UserPropsFromDispatch {
  onSubmit: () => void
  resetUserProfile: (payload: null) => void
  getUserProfile: (id: string) => void
}

type Props = ParentProps & ProfileReducer & UserPropsFromDispatch

class UserProfile extends Component<Props> {
  static mapDispatchToProps = {
    onSubmit,
    getUserProfile,
    resetUserProfile: getUserProfile.success
  }
  static mapStateFromProps = ({ user, usersList, profile }: RootState) => ({
    accountID: user.id,
    role: user.role,
    info: profile.info,
    isFetching: profile.isFetching
  })

  componentDidMount() {
    const { match, accountID } = this.props
    const id = get(match, 'params.id', accountID)
    this.props.getUserProfile(id)
  }

  componentWillUnmount() {
    this.props.resetUserProfile(null)
  }

  render() {
    if (this.props.isFetching) return <Loader />

    const { role, info, accountID } = this.props
    const initialValues = getInitValueForm(info)
    const myProfile = initialValues && accountID === initialValues.id
    const isLinkVisible = get(this.props.match, 'params.id', null)
    const titleOptions = isLinkVisible
      ? {
          linkURL: Routes.Users,
          linkTitle: 'Back to List Users'
        }
      : {}

    return (
      <div className='page-container'>
        <HeaderPageContent title='Profile' {...titleOptions} />

        <div className='page-content'>
          <ProfileForm
            onSubmit={this.props.onSubmit}
            role={role}
            initialValues={initialValues}
            myProfile={myProfile}
          />
        </div>
      </div>
    )
  }
}

export default connect(
  UserProfile.mapStateFromProps,
  UserProfile.mapDispatchToProps
)(UserProfile)
