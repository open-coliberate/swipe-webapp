import { History } from 'history'
import { routerMiddleware } from 'connected-react-router'
import { applyMiddleware, createStore, Store } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import { routinePromiseWatcherSaga } from 'redux-saga-routines'

import createRootReducer, { RootState } from './reducers'
import rootSaga from './sagas'

export function configureStore(history: History, initialState?: RootState): Store<RootState> {
  const sagaMiddleware = createSagaMiddleware()
  let middleware = applyMiddleware(routerMiddleware(history), sagaMiddleware)

  if (process.env.NODE_ENV !== 'production') {
    middleware = composeWithDevTools(middleware)
  }

  const store = createStore(
    createRootReducer(history) as any,
    initialState as any,
    middleware
  ) as Store<RootState>

  if (module['hot']) {
    module['hot'].accept('./reducers', () => {
      const nextReducer = require('./reducers')
      store.replaceReducer(nextReducer)
    })
  }

  sagaMiddleware.run(rootSaga)
  sagaMiddleware.run(routinePromiseWatcherSaga)

  return store
}
