import Cookies from 'js-cookie'
import React from 'react'
import { connect } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router'
import isBefore from 'date-fns/is_before'

import { refreshTokenRoutineActions as refreshToken } from './actions'
import Loader from './components/Loader'
import { RootState } from './reducers/state'
import { Routes } from './utils/routes'
import Application from './pages'

type Props = {
  refreshToken: ({ refreshToken }: { refreshToken: string }) => void
  isFetching: Boolean
  isAuth: boolean
} & RouteComponentProps<{}>

const checkTokenExpire = (): boolean => {
  const expiresIn = Cookies.get('expiresIn') || ''
  return expiresIn ? isBefore(new Date(), new Date(expiresIn)) : false
}

export class App extends React.Component<Props> {
  static mapStateToProps = ({ user }: RootState) => ({
    isFetching: user.isFetching,
    isAuth: user.isUserAuth
  })

  static mapDispatchToProps = {
    refreshToken
  }

  componentDidMount() {
    const { history, location } = this.props
    // TODO: private routes should be rewrited
    const isPrivateRout = Boolean(location.pathname.match(/^\/app\/.*/))
    if (!Cookies.get('accessToken') && isPrivateRout) {
      history.replace(Routes.Login)
    } else {
      this.checkAndRefresh()
    }
  }

  componentDidUpdate() {
    this.checkAndRefresh()
  }

  checkAndRefresh = () => {
    const needsRefresh = !checkTokenExpire()
    const isPrivateRout = Boolean(this.props.location.pathname.match(/^\/app\/.*/))
    if (needsRefresh && isPrivateRout) {
      const { history, isFetching } = this.props
      const refreshToken: string | undefined = Cookies.get('refreshToken')
      refreshToken && !isFetching
        ? this.props.refreshToken({ refreshToken })
        : isPrivateRout && history.replace(Routes.Login)
    }
  }

  render() {
    const { isAuth, isFetching } = this.props
    const isLoading = !isAuth && isFetching
    return (
      <div className='main-container'>
        <Application isAuth={isAuth} />
        {isLoading ? <Loader /> : null}
      </div>
    )
  }
}

export default withRouter(
  connect(
    App.mapStateToProps,
    App.mapDispatchToProps
  )(App)
)
