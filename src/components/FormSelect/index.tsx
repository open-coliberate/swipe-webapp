import './style.css'

import * as React from 'react'
import Select from 'react-select'

const FormSelect = ({
  input,
  options,
  name,
  id,
  className,
  isClearable,
  defaultInputValue,
  meta: { touched, error, warning },
  isMulti
}: any) => (
  <React.Fragment>
    <Select
      {...input}
      id={id}
      name={name + '1'}
      options={options}
      className={className}
      defaultInputValue={defaultInputValue}
      isMulti={isMulti}
      value={input.value}
      onChange={value => input.onChange(value)}
      isClearable={isClearable}
      onBlur={(value: React.FocusEvent<HTMLElement>) => input.onBlur(input.value)}
    />
    {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
  </React.Fragment>
)

export default FormSelect
