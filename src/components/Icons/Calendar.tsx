import * as React from 'react'

export default function CalendarIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      {/* <use   /> */}
      <path
        id='a'
        fill='#5B5B5B'
        fillRule='evenodd'
        d='M19.2 17.404v-6.417H4.8v6.417A1.8 1.8 0 0 0 6.599 19.2H17.4a1.8 1.8 0 0 0 1.799-1.796zM18.126 4.873A3.6 3.6 0 0 1 21 8.396v9.008A3.598 3.598 0 0 1 17.401 21H6.6A3.599 3.599 0 0 1 3 17.405v-9.01A3.6 3.6 0 0 1 6.599 4.8h1.35v-.9a.899.899 0 1 1 1.8 0v.9h4.501v-.9a.899.899 0 1 1 1.8 0v.9h1.351c.248 0 .49.025.725.073z'
      />
    </svg>
  )
}
