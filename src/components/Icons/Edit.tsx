import * as React from 'react'

export default function EditIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <path
        fill='#B7BBCB'
        fill-rule='evenodd'
        d='M16.944 6.546C15.833 5.436 15 5.603 15 5.603l-3.891 3.886-4.447 4.44-.778 3.665 3.668-.777 4.447-4.442 3.891-3.885s.167-.834-.945-1.944zm-7.611 9.83l-1.25.269a2.853 2.853 0 0 0-.533-.717 2.874 2.874 0 0 0-.718-.531l.27-1.25.361-.36s.681.013 1.45.78c.767.768.781 1.448.781 1.448l-.361.361z'
      />
    </svg>
  )
}
