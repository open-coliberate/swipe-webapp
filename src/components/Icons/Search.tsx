import * as React from 'react'

export default function SearchIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <g fill='#4A90E2' fill-rule='evenodd'>
        <path d='M14.407 13.39c.234-.234.66-.19.85 0l3.682 3.683a.601.601 0 0 1 0 .85c-.234.234-.66.19-.85 0l-3.682-3.683a.601.601 0 0 1 0-.85z' />
        <path d='M16.218 10.61A5.61 5.61 0 1 0 5 10.608a5.61 5.61 0 0 0 11.218 0zm-1.202 0a4.407 4.407 0 1 0-8.814-.001 4.407 4.407 0 0 0 8.814 0z' />
      </g>
    </svg>
  )
}
