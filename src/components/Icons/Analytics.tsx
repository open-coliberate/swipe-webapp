import * as React from 'react'

export default function AnalyticsIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <path
        fill='none'
        fillRule='evenodd'
        stroke='#919191'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='1.857'
        d='M4.874 21.692V4h6.036v17.692H22 4.874zm9.542 0V10.644h5.71v11.048h-5.71z'
      />
    </svg>
  )
}
