import * as React from 'react'

export default function EyeCrossedIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <g
        fill='none'
        fill-rule='evenodd'
        stroke='#FFC000'
        stroke-linecap='round'
        stroke-linejoin='round'
        stroke-width='2.063'
      >
        <path d='M23 12s-4.925 8-11 8-11-8-11-8 4.925-8 11-8 11 8 11 8z' />
        <path d='M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM1 23L23 1' />
      </g>
    </svg>
  )
}
