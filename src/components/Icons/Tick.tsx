import * as React from 'react'

export default function TickIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <g fill='none' fill-rule='evenodd'>
        <circle cx='12' cy='12' r='10' fill='#1AB14F' fillRule='nonzero' />
        <path
          stroke='#FFF'
          strokeLinecap='round'
          strokeLinejoin='round'
          strokeWidth='2'
          d='M5 12.012L9.659 17 19 7'
        />
      </g>
    </svg>
  )
}
