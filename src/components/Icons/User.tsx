import React from 'react'

export default function UserIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <g fill='none' fillRule='evenodd'>
        <path
          stroke='#919191'
          strokeWidth='2'
          d='M6.994 12H17A3.002 3.002 0 0 1 20 14.998v2.992c0 1.11-.897 2.01-2.007 2.01H6.007A2.003 2.003 0 0 1 4 17.99v-2.992A2.998 2.998 0 0 1 6.994 12z'
        />
        <path
          fill='#919191'
          fillRule='nonzero'
          d='M12 13a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm0 2a6 6 0 1 1 0-12 6 6 0 0 1 0 12z'
        />
      </g>
    </svg>
  )
}
