import * as React from 'react'

export default function SelectIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <path fill='#B7BBCB' fill-rule='evenodd' d='M8 10h8.485l-4.242 4.243z' />
    </svg>
  )
}
