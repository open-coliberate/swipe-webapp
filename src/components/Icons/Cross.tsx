import * as React from 'react'

export default function CrossIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <path
        fill='#FFF'
        fillRule='evenodd'
        d='M14.296 8.291a1.002 1.002 0 0 1 1.413 0 1.002 1.002 0 0 1 0 1.413L13.413 12l2.296 2.296a1.002 1.002 0 0 1 0 1.413 1.002 1.002 0 0 1-1.413 0L12 13.413l-2.296 2.296a1.002 1.002 0 0 1-1.413 0 1.002 1.002 0 0 1 0-1.413L10.587 12 8.291 9.704a1.002 1.002 0 0 1 0-1.413 1.002 1.002 0 0 1 1.413 0L12 10.587l2.296-2.296z'
      />
    </svg>
  )
}
