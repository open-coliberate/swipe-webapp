import * as React from 'react'

export default function HelloIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='40' height='40' viewBox='0 0 40 40'>
      <text
        fill='#FFC000'
        fill-rule='evenodd'
        font-family='OpenSans-Bold, Open Sans'
        font-size='32'
        font-weight='bold'
        transform='translate(-6)'
      >
        <tspan x='1.844' y='34'>
          {' '}
        </tspan>{' '}
        <tspan
          x='10.156'
          y='34'
          font-family='AppleColorEmoji, Apple Color Emoji'
          font-weight='normal'
        >
          👋
        </tspan>
      </text>
    </svg>
  )
}
