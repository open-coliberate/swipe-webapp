import * as React from 'react'

export default function BellIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <path
        fill='#FFF'
        fillRule='nonzero'
        d='M17.955 16l-1.099-7.785C16.533 5.925 14.316 4 11.999 4 9.685 4 7.464 5.926 7.142 8.215L6.042 16h11.913zM5.16 7.935C5.624 4.657 8.691 2 12 2c3.313 0 6.375 2.66 6.837 5.935l1.142 8.087c.154 1.092-.61 1.978-1.713 1.978H5.733c-1.1 0-1.868-.883-1.713-1.978L5.16 7.935zM15 19a3 3 0 1 1-6 0h1.5a1.5 1.5 0 0 0 3 0h1.5z'
      />
    </svg>
  )
}
