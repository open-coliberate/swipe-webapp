import * as React from 'react'

export default function ArrowIcon() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
      <path
        fill='#fff'
        fillRule='evenodd'
        d='M8.857 6.466c-.242.178-.275.494-.073.708L13.34 12l-4.556 4.826a.464.464 0 0 0 .073.708c.241.179.6.15.802-.063l4.86-5.149a.468.468 0 0 0 .132-.322.468.468 0 0 0-.132-.322L9.66 6.529a.623.623 0 0 0-.802-.063z'
      />
    </svg>
  )
}
