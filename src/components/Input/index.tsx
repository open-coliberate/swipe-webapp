import './style.css'

import React from 'react'
import Ico from '../Ico'

interface InputProps {
  type?: string
  className?: string
  placeholder?: string
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void
  value?: string
  icoPosition?: string
  icoType?: string
  id?: string
}

const Input: React.StatelessComponent<InputProps> = ({
  type,
  className,
  placeholder,
  onChange,
  icoPosition,
  icoType,
  value,
  id
}) => {
  const cn = `input-container ${className}`

  return (
    <div className={cn}>
      <Ico isVisible={icoPosition === 'before'} icoType={icoType} icoPosition='before' />
      <input
        type={type}
        className='form-field-input'
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        id={id || ''}
      />
    </div>
  )
}

Input.defaultProps = {
  type: 'text',
  className: ''
}

export default Input
