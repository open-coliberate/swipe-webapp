import React, { PureComponent, createRef } from 'react'
import classNames from 'classnames'
import { TimeLabel } from '../../utils/helper'

type Props = {
  timeLabel: Array<TimeLabel>
  workingTime: Array<boolean>
  changeWorkingTime: (arg: Array<boolean>) => void
  closeList: () => void
}

type State = {
  selectedTime: Array<boolean>
}

const body = document.getElementsByTagName('BODY')[0]

class List extends PureComponent<Props, State> {
  state = {
    selectedTime: this.props.workingTime
  }

  listRef = createRef<HTMLUListElement>()

  componentDidMount() {
    body.addEventListener('click', this.bodyListener, false)
  }
  componentWillUnmount() {
    body.removeEventListener('click', this.bodyListener, false)
    this.props.changeWorkingTime(this.state.selectedTime)
  }

  bodyListener = (e: Event) => {
    const ul = this.listRef.current
    const parent = (e.target as HTMLElement).parentElement
    const isSelect = parent && (parent === ul || parent.parentElement === ul)
    if (!isSelect) this.props.closeList()
  }

  changeTime = (idx: number) => {
    this.setState(({ selectedTime }) => ({
      selectedTime: selectedTime.map((el, i) => (i === idx ? !el : el))
    }))
  }

  render() {
    const { timeLabel } = this.props
    const { selectedTime } = this.state

    return (
      <ul className='wt-list' ref={this.listRef}>
        {timeLabel.map((el, i) => {
          const cn = classNames('wt-item', { selected: selectedTime[i] })
          return (
            <li
              key={el.value}
              className={cn}
              onClick={(e: React.SyntheticEvent) => {
                e.preventDefault()
                this.changeTime(i)
              }}
            >
              <span className='wt-item__label'>{el.label}</span>
              <span className='wt-item__ico' />
            </li>
          )
        })}
      </ul>
    )
  }
}

export default List
