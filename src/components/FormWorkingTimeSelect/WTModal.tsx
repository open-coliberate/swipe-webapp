import React, { useState } from 'react'
import WTModalHeader from './WTModalHeader'
import WTModalBody, { WeakState } from './WTModalBody'
import Button from '../Button'

type Props = {
  handlerCancel: () => void
  handlerApply: (arg: any) => void
}

type State = {
  data: WeakState[]
  dirty: boolean
}

const WorkingTimeModal: React.StatelessComponent<Props> = ({ handlerCancel, handlerApply }) => {
  const [state, changeData] = useState<State>({ data: [], dirty: false })

  const applyClick = () => {
    handlerApply(state.data)
    handlerCancel()
  }

  return (
    <div className='modal-wh'>
      <div className='modal-title'>Hours of work</div>
      <div className='modal-body'>
        <WTModalHeader />
        <WTModalBody changeData={data => changeData({ data, dirty: true })} />
      </div>

      <div className='controls flex-right'>
        <div className='controls__item'>
          <Button className='btn-small --grey' onClick={handlerCancel} children='Cancel' />
        </div>
        <div className='controls__item'>
          <Button
            className='btn-small'
            disabled={!state.dirty}
            onClick={applyClick}
            children='Apply'
          />
        </div>
      </div>
    </div>
  )
}

export default WorkingTimeModal
