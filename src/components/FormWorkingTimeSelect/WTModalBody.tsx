import React, { useState } from 'react'
import Select from 'react-select'

import ListContainer from './ListContainer'
import { TimeLabel } from '../../utils/helper'

export interface WeakState {
  worksTime: boolean[]
  weekDay: boolean
}

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
const options = [
  {
    label: 'Day off',
    value: '0'
  },
  {
    label: 'Works',
    value: '1'
  }
]
const worksTime = [
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  true,
  true,
  true,
  true,
  true,
  true,
  true,
  true,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false
]
const initialState = [
  {
    // Sunday
    worksTime: [],
    weekDay: false
  },
  {
    // Monday
    worksTime,
    weekDay: true
  },
  {
    // Tuesday
    worksTime,
    weekDay: true
  },
  {
    // Wednesday
    worksTime,
    weekDay: true
  },
  {
    // Thursday
    worksTime,
    weekDay: true
  },
  {
    // Friday
    worksTime,
    weekDay: true
  },
  {
    // Saturday
    worksTime: [],
    weekDay: false
  }
]
interface WTModalBodyProps {
  changeData: (state: Array<WeakState>) => void
}

const WTModalBody: React.FunctionComponent<WTModalBodyProps> = ({ changeData }) => {
  const [weakState, changeWeakState] = useState<Array<WeakState>>(initialState)

  const handlerDayTypeChange = (idx: number, key: string, val: TimeLabel | Array<boolean>) => {
    const state = weakState.map((el, i) =>
      i !== idx
        ? el
        : {
            ...el,
            [key]: 'value' in val ? Boolean(+val.value) : val
          }
    )
    changeWeakState(state)
    changeData(state)
  }

  return (
    <div className='modal-wh__body'>
      {days.map((el, i) => (
        <div className='wh__row' key={el}>
          <div className='wh__cell'>{el}</div>
          <div className='wh__cell'>
            <Select
              options={options}
              defaultValue={options.find(el => el.value === String(+weakState[i].weekDay))}
              onChange={(value: TimeLabel) => handlerDayTypeChange(i, 'weekDay', value)}
            />
          </div>
          <div className='wh__cell'>
            {weakState[i].weekDay ? (
              <ListContainer
                workingTime={weakState[i].worksTime}
                changeWorkingTime={(val: Array<boolean>) =>
                  handlerDayTypeChange(i, 'worksTime', val)
                }
              />
            ) : null}
          </div>
        </div>
      ))}
    </div>
  )
}

export default WTModalBody
