import React from 'react'

const header = ['Day of Weak', 'Type', 'Work Hours']

const WTModalHeader = () => (
  <div className='modal-wh__header wh__row'>
    {header.map((el, key) => (
      <div className='wh__cell' key={`${el}_${key}`}>
        {el}
      </div>
    ))}
  </div>
)

export default WTModalHeader
