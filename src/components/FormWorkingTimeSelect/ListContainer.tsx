import React, { useState } from 'react'
import classNames from 'classnames'
import { timeHourGenerator, timeRangeFormatter } from '../../utils/helper'
import List from './List'

import './style.css'

type Props = {
  workingTime: Array<boolean>
  changeWorkingTime: (arg: Array<boolean>) => void
}

const timeLabel = timeHourGenerator()

const ListContainer: React.FunctionComponent<Props> = ({ workingTime, changeWorkingTime }) => {
  const [selectIsOpen, selectToggle] = useState<boolean>()

  const handlerToggleSelect = () => selectToggle(state => !state)

  const time = timeLabel.filter(({}, i) => workingTime[i])
  const workingHoursInterval = timeRangeFormatter(time)
  const bc = classNames('wt-select', { open: selectIsOpen })

  return (
    <div className={bc}>
      <div className='wt-input' onClick={handlerToggleSelect}>
        {workingHoursInterval.map(({ from, to }) => (
          <span key={from.value}>{`${from.label} - ${to ? to.label : ''}`}</span>
        ))}
        <i className='wt-input__arrow' />
      </div>
      {selectIsOpen ? (
        <List
          timeLabel={timeLabel}
          workingTime={workingTime}
          changeWorkingTime={changeWorkingTime}
          closeList={handlerToggleSelect}
        />
      ) : null}
    </div>
  )
}

export default ListContainer
