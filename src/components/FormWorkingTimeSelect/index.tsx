import React, { Component } from 'react'
import { WrappedFieldProps } from 'redux-form'
import flatMapDeep from 'lodash/flatMapDeep'

import WorkingTimeModal from './WTModal'
import { WeakState } from './WTModalBody'
import Modal from '../../pages/Modal'
import { APIWorkingHoursType } from '../../models'

const slotTemplate = {
  deleted: false,
  from: 0,
  to: 0,
  day: 0,
  isWorking: true,
  space: '',
  id: null
}
const convertValueFormToAPI = (value: WeakState[]) => {
  const workingHours = value.map((el, dayOfWeak: number) => {
    const wh = el.worksTime.map((el, i) =>
      el
        ? {
            ...slotTemplate,
            from: i,
            to: 1 + i,
            day: dayOfWeak,
            space: 'fd1f37f8-c107-4c60-8365-33ee180f6d0b'
          }
        : el
    ) as (boolean | APIWorkingHoursType)[]

    const newArr = wh.reduce((prev: APIWorkingHoursType[], curr: APIWorkingHoursType) => {
      if (curr) {
        if (!prev.length) prev.push(curr)
        else {
          const last: APIWorkingHoursType = prev.slice(-1)[0]
          if (last.to === curr.from) last.to = curr.to
          else {
            prev.push(curr)
          }
        }
      }
      return prev
    }, [])

    return newArr.map(el => {
      el.from = el.from * 3600
      el.to = el.to * 3600
      return el
    })
  })
  return flatMapDeep(workingHours) as APIWorkingHoursType[]
}

interface ModalWorkingTimeState {
  modalShow: boolean
  value: WeakState[]
}
type Props = WrappedFieldProps & typeof defaultProps

const defaultProps = {
  label: 'Hours of work',
  placeholder: 'Hours of work'
}

const cn = 'working-time'

class WorkingTime extends Component<Props, ModalWorkingTimeState> {
  static defaultProps = defaultProps

  state = {
    modalShow: false,
    value: []
  }

  handlerApply = (value: WeakState[]) => {
    this.setState({ value })
    const formatedValue = convertValueFormToAPI(value)
    this.props.input.onChange(formatedValue)
  }

  handlerClose = () => this.setState({ modalShow: false })
  handlerClick = () => this.setState(state => ({ modalShow: !state.modalShow }))

  render() {
    const { placeholder, label } = this.props
    return (
      <div className='form-field'>
        <div className='heading'>{label}</div>
        <div className={cn} onClick={this.handlerClick}>
          {placeholder}
        </div>
        {this.state.modalShow ? (
          <Modal closeModal={this.handlerClose} className='--dark'>
            <WorkingTimeModal handlerCancel={this.handlerClose} handlerApply={this.handlerApply} />
          </Modal>
        ) : null}
      </div>
    )
  }
}

export default WorkingTime
