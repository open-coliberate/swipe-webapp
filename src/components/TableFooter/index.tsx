import React from 'react'
import Select from 'react-select'
import './style.css'

function selectOptions(limit?: number) {
  if (!limit) return [{ value: '5', label: 5 }]
  const filteredArr = [5, 10, 20, 25, 50, 100].filter(el => limit >= el)
  return (filteredArr.length ? filteredArr : [limit]).map(el => ({ value: `${el}`, label: el }))
}

interface FooterProps {
  pageSizeOptions: Array<number>
  pageSize: number
  page: number
}

type RSC = React.StatelessComponent<FooterProps>

const Footer: RSC = props => {
  const { pageSize, page } = props
  const arrOptions = selectOptions(pageSize)
  const defaultValue = arrOptions.find(el => +el.value === pageSize)
  const prevBtnCN = `pagination__btn${page === 0 ? ' disabled' : ''}`

  return (
    <div className='table-footer'>
      <div className='table-footer__item'>{pageSize} Items</div>
      <div className='table-footer__item pagination'>
        <span className={prevBtnCN}>First</span>
        <span className={prevBtnCN}>Prev</span>
        <span className='current-page'>{page + 1}</span>
        <span className='pagination__btn'>Next</span>
        <span className='pagination__btn'>Last</span>
      </div>
      <div className='table-footer__item'>
        <span>Show</span>
        <Select
          defaultValue={defaultValue}
          className='select'
          isDisabled={arrOptions.length === 1}
          options={arrOptions}
        />
      </div>
    </div>
  )
}

export default Footer
