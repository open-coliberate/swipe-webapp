import React, { useState, useRef } from 'react'

import './style.css'

interface MoreDefaultProps {
  className?: '--lite' | '--dark'
  id?: string
  children: React.ReactElement | null
}

export const MoreDefault: React.StatelessComponent<MoreDefaultProps> = ({
  className,
  children
}) => {
  const [isMenuShow, toggleMenu] = useState<boolean>(false)
  const backSheet = useRef(null)
  const cn = `more__ico ${className}`

  const handlerToggleMenu = (e: React.SyntheticEvent) => {
    e.preventDefault()
    e.stopPropagation()
    toggleMenu(state => !state)
  }

  const handlerMenuItem = (e: React.SyntheticEvent) => {
    e.preventDefault()
    e.stopPropagation()
    backSheet.current === e.target && toggleMenu(false)
  }

  return (
    <div className='more'>
      <i className={cn} onClick={handlerToggleMenu} />
      {isMenuShow ? (
        <div className='more-menu'>
          <div className='more-menu__back-sheet' ref={backSheet} onClick={handlerMenuItem} />
          <div className='more-menu__content'>{children}</div>
        </div>
      ) : null}
    </div>
  )
}

MoreDefault.defaultProps = {
  className: '--dark'
}
