import React, { useState } from 'react'
import Checkbox from '../Checkbox'
import { PromoteUserProps } from '../../pages/Users/UsersList'

export interface MoreListProps {
  id: string
  role: string
  orgID: string
  handlerCallback?: (arg: PromoteUserProps) => void
  menuList?: {
    name: string
    role: string
  }[]
}

interface ItemProps {
  key: string
  name: string
  role: string
  payload: PromoteUserProps
  handlerCallback?: (arg: PromoteUserProps) => void
}

type RFCItem = React.FunctionComponent<ItemProps>
type RSCList = React.StatelessComponent<MoreListProps>

const MenuItem: RFCItem = ({ name, role, payload, handlerCallback }) => {
  const [isChecked, changeStatus] = useState(role === payload.role)

  const handlerClick = (e: React.SyntheticEvent) => {
    e.preventDefault()
    changeStatus(state => !state)
    handlerCallback && handlerCallback(payload)
  }

  return (
    <div className='menu__item' onClick={handlerClick}>
      <span className='item__text'>{name}</span>
      <Checkbox value={payload.role} className='--white' checked={isChecked} />
    </div>
  )
}

const MoreList: RSCList = ({ menuList, id, role, orgID, handlerCallback }) => {
  if (!menuList) return null
  return (
    <>
      {menuList.map(el => {
        const payload = {
          id,
          role: el.role,
          orgID
        }
        return (
          <MenuItem
            key={`${el.role}`}
            name={el.name}
            role={role}
            payload={payload}
            handlerCallback={handlerCallback}
          />
        )
      })}
    </>
  )
}

export default MoreList
