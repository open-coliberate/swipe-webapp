import React, { useState, useRef } from 'react'
import MoreList, { MoreListProps } from './MoreList'
export { MoreDefault } from './MoreDefault'

import './style.css'

interface MoreProps extends MoreListProps {
  className?: string
}

const More: React.StatelessComponent<MoreProps> = ({
  className,
  menuList,
  role,
  id,
  orgID,
  handlerCallback
}) => {
  const [isMenuShow, toggleMenu] = useState<boolean>(false)
  const inputEl = useRef(null)
  const cn = `more__ico ${className}`

  const handlerToggleMenu = (e: React.SyntheticEvent) => {
    e.preventDefault()
    toggleMenu(state => !state)
  }

  const handlerMenuItem = (e: React.SyntheticEvent) => {
    e.preventDefault()
    inputEl.current === e.target && toggleMenu(false)
  }

  return (
    <div className='more'>
      <i className={cn} onClick={handlerToggleMenu} />
      {isMenuShow && menuList && menuList.length ? (
        <div className='more-menu'>
          <div className='more-menu__back-sheet' ref={inputEl} onClick={handlerMenuItem} />
          <MoreList
            role={role}
            id={id}
            orgID={orgID}
            menuList={menuList}
            handlerCallback={handlerCallback}
          />
        </div>
      ) : null}
    </div>
  )
}

More.defaultProps = {
  className: '--dark',
  menuList: []
}

export default More
