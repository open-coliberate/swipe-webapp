import React from 'react'
import Select from 'react-select'
import { WrappedFieldProps } from 'redux-form'
import CrossIcon from '../Icons/Cross'
import './style.css'

interface ToolSelectProps {
  options: OptionType[]
}
interface OptionType {
  label: string
  value: string
}
interface ReduceProps {
  selectOptions: OptionType[]
  selected: OptionType[]
}

type ToolSelectType = ToolSelectProps & WrappedFieldProps

const toolSelectBeauty = (options: OptionType[], inputValue: string[]) =>
  options.reduce(
    (prev: ReduceProps, next: OptionType) => {
      const isSelected = inputValue.includes(next.value)
      return {
        selectOptions: isSelected ? prev.selectOptions : prev.selectOptions.concat(next),
        selected: isSelected ? prev.selected.concat(next) : prev.selected
      }
    },
    {
      selectOptions: [],
      selected: []
    }
  )

const ToolSelect: React.FunctionComponent<ToolSelectType> = ({ options, input }) => {
  const inputValue: string[] = input.value

  const handlerChange = (val: OptionType) => {
    const isAlreadySelected = inputValue.some(toolID => toolID === val.value)
    const newArr = isAlreadySelected
      ? inputValue.filter(toolID => toolID !== val.value)
      : inputValue.concat(val.value)
    input.onChange(newArr)
  }

  const { selectOptions, selected } = toolSelectBeauty(options, inputValue)

  return (
    <>
      <div className='modal-body__row'>
        <span className='cell__label'>Tools used during the event</span>
        <Select
          className='select full-width'
          options={selectOptions}
          onChange={handlerChange}
          value={null}
        />
      </div>
      <div className='selected-list'>
        {selected.map(el => (
          <span className='selected__item' key={el.value}>
            {el.label}
            <span className='select__item-ico' onClick={e => handlerChange(el)}>
              <CrossIcon />
            </span>
          </span>
        ))}
      </div>
    </>
  )
}

export default ToolSelect
