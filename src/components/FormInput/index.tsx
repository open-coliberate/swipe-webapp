import React from 'react'
import { WrappedFieldProps } from 'redux-form'
import classnames from 'classnames'

interface FormInputWithForms extends WrappedFieldProps {
  type: string
  className?: string
  label?: string
  placeholder?: string
  disabled?: boolean
  autoComplete?: 'off' | 'on'
}
type Props = FormInputWithForms & typeof defaultProps

const defaultProps = {
  type: 'text',
  autoComplete: 'off'
}

const inputWarn = 'form-field-warn'
const FormInput = ({
  input,
  type,
  disabled,
  meta: { touched, error, warning },
  className,
  label,
  placeholder,
  autoComplete
}: Props) => {
  const cn = classnames('form-field', className)
  return (
    <div className={cn}>
      {label ? <div className='form-field-label'>{label}</div> : null}
      <input
        {...input}
        autoComplete={autoComplete}
        type={type}
        disabled={disabled}
        className='form-field-input'
        placeholder={placeholder}
      />
      {touched &&
        ((error && <span className={inputWarn}>{error}</span>) ||
          (warning && <span className={inputWarn}>{warning}</span>))}
    </div>
  )
}
FormInput.defaultProps = defaultProps

export default FormInput
