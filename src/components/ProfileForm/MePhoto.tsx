import * as React from 'react'

export interface MyPhotoProps {
  src: any
  handleChange: () => void
}

export class MyPhoto extends React.PureComponent<MyPhotoProps, any> {
  render() {
    const { src, handleChange } = this.props
    return (
      <React.Fragment>
        <span className='heading'>Profile photo</span>
        <div className='profile-pic'>
          <img className='profile-pic-photo' src={src} alt='mypic' />
          <input className='profile-pic-input' type='file' onChange={() => handleChange()} />
        </div>
      </React.Fragment>
    )
  }
}
