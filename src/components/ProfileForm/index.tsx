import React from 'react'
import { reduxForm, Form, Field, FieldArray, InjectedFormProps } from 'redux-form'
import { Dispatch } from 'redux'
import get from 'lodash/get'

import { MyPhoto } from './MePhoto'
import WorkingTime from '../FormWorkingTimeSelect'
import ToolsList from '../FormTools'
import { FormTextarea } from '../../components/FormTextarea'
import FormInput from '../../components/FormInput'
import Button from '../../components/Button'
import { Roles, APIWorkingHoursType } from '../../models'
import defaultsrc from '../../utils/assets/defuserpic.png'
import './style.css'

type Tools = {
  id: string
  title: string
  description: string
}
export type UserInfo = {
  id: string
  email: string
  firstName: string
  lastName: string
  phone?: string
  description?: string
  password?: string
  passwordConfirmation?: string
  toolsAllowed?: Tools[]
  toolsDisallowed?: Tools[]
  workingHours: APIWorkingHoursType[]
}
interface ProfileProps {
  role: string
  myProfile?: boolean
  onSubmit: (data: UserInfo, dispatch: Dispatch<any>, props: ProfileProps) => void
}

type Props = ProfileProps & InjectedFormProps<UserInfo, ProfileProps>

const Profile: React.StatelessComponent<Props> = props => {
  const { pristine, reset, handleSubmit, role, myProfile } = props
  const hasAdditionalField =
    role === Roles.LAB_MANAGER || role === Roles.SUPER_USER || role === Roles.OWNER
  const hasEditPermission = myProfile || role === Roles.LAB_MANAGER || role === Roles.OWNER
  const hasTools =
    get(props, 'initialValues.toolsAllowed', false) ||
    get(props, 'initialValues.toolsDisallowed', false)

  return (
    <div className='page-content'>
      <Form onSubmit={handleSubmit}>
        <div className='profile-container'>
          <div className='form-row'>
            <div className='half'>
              <Field
                name='firstName'
                placeholder='First Name'
                label='First Name'
                disabled={!hasEditPermission}
                component={FormInput}
              />
              <Field
                name='lastName'
                placeholder='Last Name'
                label='Last Name'
                disabled={!hasEditPermission}
                component={FormInput}
              />
              <Field
                type='email'
                name='email'
                disabled
                placeholder='Email'
                label='Email Address'
                component={FormInput}
              />
              <Field
                name='phone'
                placeholder='Phone number'
                label='Phone Number'
                disabled={!hasEditPermission}
                component={FormInput}
              />
              <Field type='hidden' name='id' component={FormInput} />
              {hasAdditionalField ? (
                <Field name='workingHours' disabled={!myProfile} component={WorkingTime} />
              ) : null}
            </div>
            <div className='half'>
              <div className='form-field'>
                <MyPhoto src={defaultsrc} handleChange={() => console.log('inputed something')} />
              </div>
              <Field
                name='description'
                label='Notes'
                component={FormTextarea}
                disabled={role !== Roles.LAB_MANAGER}
              />
            </div>
          </div>
        </div>
        {hasEditPermission ? (
          <div className='container'>
            <div className='form-row'>
              <Field
                className='half'
                type='password'
                name='oldPassword'
                placeholder='Old Password'
                label='Change password'
                component={FormInput}
              />
            </div>
            <div className='form-row'>
              <Field
                className='half'
                type='password'
                name='password'
                placeholder='New Password'
                label='New Password'
                component={FormInput}
              />
              <Field
                className='half'
                type='password'
                name='passwordConfirmation'
                placeholder='Confirm Password'
                label='Confirm New Password'
                component={FormInput}
              />
            </div>
          </div>
        ) : null}

        {hasTools ? (
          <div className='profile-container tools-block'>
            <div className='form-field-label'>Trainings</div>
            <FieldArray
              name='toolsAllowed'
              component={ToolsList}
              fieldsAction={props.array}
              disabled={!hasAdditionalField}
            />

            <div className='form-field-label'>Without access</div>
            <FieldArray
              name='toolsDisallowed'
              component={ToolsList}
              fieldsAction={props.array}
              disabled={!hasAdditionalField}
            />
          </div>
        ) : null}

        {hasEditPermission ? (
          <div className='controls flex-right'>
            <div className='controls__item'>
              <Button className='btn-small --grey' onClick={reset} children='Cancel' />
            </div>
            <div className='controls__item'>
              <Button
                className='btn-small'
                type='submit'
                disabled={pristine}
                children='Save changes'
              />
            </div>
          </div>
        ) : null}
      </Form>
    </div>
  )
}

export default reduxForm<UserInfo, ProfileProps>({
  form: 'profile'
})(Profile)
