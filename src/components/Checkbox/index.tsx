import React from 'react'
import CheckboxImg from '../Icons/Checkbox'
import './style.css'

interface CheckboxProps {
  value: string
  type?: string
  className?: string
  checked?: boolean
  onClick?: (e: React.MouseEvent<HTMLElement>) => void
}

const Checkbox: React.StatelessComponent<CheckboxProps> = ({
  className,
  type,
  checked,
  value,
  onClick
}) => {
  const cn = `checkbox ${className}`
  const color = className && ~className.indexOf('--white') ? '#fff' : undefined

  return (
    <div className='checkbox-container' onClick={onClick || undefined}>
      <input className={cn} type={type} value={value} checked={checked} onChange={() => ''} />
      <label className='checkbox-label'>
        <CheckboxImg color={color} />
      </label>
    </div>
  )
}

Checkbox.defaultProps = {
  className: '--grey',
  type: 'checkbox',
  checked: false
}

export default Checkbox
