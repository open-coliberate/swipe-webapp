import './style.css'

import * as React from 'react'
import Switch from 'react-switch'

// interface ToggleProps {
//   className?: string;
//   onChange?: any;
//   value?: any;
// }

export const FormToggle = (props: any) => (
  <div className={props.className}>
    <Switch {...props} onChange={props.input.onChange} checked={Boolean(props.input.value)} />
  </div>
)
