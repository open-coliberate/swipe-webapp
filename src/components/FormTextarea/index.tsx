import * as React from 'react'
import { WrappedFieldProps } from 'redux-form'
import classnames from 'classnames'

interface FormTextareaProps extends WrappedFieldProps {
  className: string
  placeholder?: string
  label?: string
  disabled?: boolean
}

const inputWarn = 'form-field-warn'

export const FormTextarea = ({
  input,
  meta: { touched, error, warning },
  className,
  placeholder,
  label,
  disabled
}: FormTextareaProps) => {
  const cn = classnames('form-field', className)
  return (
    <div className={cn}>
      {label ? <div className='form-field-label'>{label}</div> : null}
      <textarea
        {...input}
        disabled={disabled}
        className='form-field-textarea'
        placeholder={placeholder}
      />
      {touched &&
        ((error && <span className={inputWarn}>{error}</span>) ||
          (warning && <span className={inputWarn}>{warning}</span>))}
    </div>
  )
}
