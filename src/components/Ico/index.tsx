import React from 'react'

const availableIco = ['pencil', 'bin', 'plus', 'search']

interface IcoProps {
  isVisible: boolean
  icoPosition?: 'before' | 'after' | ''
  icoType?: string
}

const Ico: React.StatelessComponent<IcoProps> = ({ isVisible, icoType, icoPosition }) => {
  const hasIco = icoType && ~availableIco.indexOf(icoType)
  return isVisible && hasIco ? <i className={`ico ico-${icoPosition} --${icoType}`} /> : null
}

Ico.defaultProps = {
  icoPosition: 'before',
  icoType: ''
}

export default Ico
