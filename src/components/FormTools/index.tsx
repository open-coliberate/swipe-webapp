import React from 'react'
import { Field } from 'redux-form'
import Tool, { ToolProp, OptionsProps } from './Tool'

import './styles.css'

const handleChange = ({
  arg,
  index,
  item,
  fieldsAction
}: {
  arg: any
  index: number
  item: ToolProp
  fieldsAction: any
}) => {
  const [e, value] = arg
  const newItem = { ...item, value }
  const [from, to] = value
    ? ['toolsDisallowed', 'toolsAllowed']
    : ['toolsAllowed', 'toolsDisallowed']

  e.preventDefault()
  fieldsAction.push(to, newItem)
  fieldsAction.remove(from, index)
}

const ToolsList: React.FunctionComponent<any> = ({ fields, disabled, fieldsAction }) => {
  return (
    <div className='tools-list'>
      {fields.map((el: OptionsProps, i: number) => (
        <Field
          key={`${i}`}
          name={`${el}.value`}
          options={fields.get(i)}
          component={Tool}
          disabled={disabled}
          groupName={fields.name}
          onChange={(...arg: any) => {
            handleChange({ arg, index: i, item: fields.get(i), fieldsAction })
          }}
        />
      ))}
    </div>
  )
}

export default ToolsList
