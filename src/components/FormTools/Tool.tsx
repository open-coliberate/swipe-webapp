import React, { ReactElement } from 'react'
import DeleteRound from '../Icons/DelRound'

export type OptionsProps = {
  id: string
  title: string
  description: string
  value: boolean
}

export type ToolProp = {
  input: any
  options: OptionsProps
  groupName: string
  disabled?: boolean
}

const icoRender = ({ groupName, disabled }: { groupName: string; disabled: boolean }) => {
  let Ico: string | null | ReactElement = null
  if (groupName === 'toolsDisallowed') {
    Ico = disabled ? null : 'Add'
  } else {
    Ico = disabled ? 'Yes' : <DeleteRound />
  }
  return Ico !== null ? <b className='tool__ico'>{Ico}</b> : null
}

const Tool = ({ input, options, disabled = false, groupName }: ToolProp) => {
  return (
    <div className='tool__item'>
      <input
        {...input}
        type='checkbox'
        id={input.name}
        checked={input.value}
        className='tool__input'
        disabled={disabled}
      />
      <label className='tool__label' htmlFor={input.name} title={options.description}>
        {options.title}
        {icoRender({ groupName, disabled })}
      </label>
    </div>
  )
}

export default Tool
