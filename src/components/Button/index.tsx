import React from 'react'
import classnames from 'classnames'

import Ico from '../Ico'
import './style.css'

type Props = {
  disabled?: boolean
  type?: 'button' | 'reset' | 'submit'
  children: any
  className?: string
  icoPosition?: string
  icoType?: string
  onClick?: (e?: any) => void
}

const Button: React.StatelessComponent<Props> = ({
  disabled,
  className,
  type,
  children,
  onClick,
  icoPosition,
  icoType
}) => (
  <button
    disabled={disabled}
    className={classnames('btn', className)}
    type={type}
    onClick={onClick}
  >
    <Ico isVisible={icoPosition === 'before'} icoType={icoType} icoPosition='before' />
    <span className='btn__text'>{children}</span>
    <Ico isVisible={icoPosition === 'after'} icoType={icoType} icoPosition='after' />
  </button>
)

Button.defaultProps = {
  disabled: false,
  icoPosition: 'before',
  type: 'button'
}

export default Button
