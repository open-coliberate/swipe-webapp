import React from 'react'
import { Link } from 'react-router-dom'

interface HeaderPageContentProps {
  title: string
  linkURL?: string
  linkTitle?: string
}

const HeaderPageContent: React.StatelessComponent<HeaderPageContentProps> = ({
  title,
  linkURL,
  linkTitle
}) => (
  <div className='page-title'>
    {linkTitle && linkURL ? (
      <span className='page-breadcrumbs'>
        <i className='ico-back'>&#x3c;</i>
        <Link to={linkURL}>{linkTitle}</Link>
      </span>
    ) : null}
    <span>{title}</span>
  </div>
)

export default HeaderPageContent
