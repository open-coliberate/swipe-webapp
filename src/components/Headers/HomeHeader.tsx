import React from 'react'
import { Link } from 'react-router-dom'

import { Routes } from '../../utils/routes'

const HomeHeader: React.StatelessComponent = () => (
  <header className='header'>
    <Link className='header-title' to={Routes.Root}>
      Coliberate
    </Link>
  </header>
)

export default HomeHeader
