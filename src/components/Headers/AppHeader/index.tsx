import * as React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { UserShort } from '../../../models'
import { RootState } from '../../../reducers'
import { Routes } from '../../../utils/routes'
import HeaderCabinet from './HeaderCabinet'
import HeaderNav from './HeaderNav'
// import HeaderSearch from './HeaderSearch';

interface ComponentPropsFromDispatch {}

interface ComponentPropsFromState {
  user?: UserShort
}

type ComponentProps = ComponentPropsFromDispatch & ComponentPropsFromState

class AppHeader extends React.PureComponent<ComponentProps> {
  static mapStateToProps(state: RootState): UserShort {
    return {
      email: state.user.email,
      id: state.user.id,
      firstName: state.user.firstName,
      lastName: state.user.firstName,
      isFetching: state.user.isFetching,
      token: state.user.token
    }
  }

  static mapDispatchToProps = {}

  // private get isCalendarPage(): boolean {
  //   return this.props.location.pathname === Routes.Calendars;
  // }

  render() {
    return (
      <header className='header header-nav'>
        <Link className='header-title header-title__white' to={Routes.Root}>
          Coliberate
        </Link>
        <HeaderNav />
        <HeaderCabinet />
        {/* {this.isCalendarPage && <HeaderSearch />} */}
      </header>
    )
  }
}

export default connect(
  AppHeader.mapStateToProps,
  AppHeader.mapDispatchToProps
)(AppHeader)
