import * as React from 'react'

export default class HeaderSearch extends React.PureComponent {
  render() {
    return (
      <div className='header-nav-search'>
        <input type='text' />
      </div>
    )
  }
}
