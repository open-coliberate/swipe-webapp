import * as React from 'react'
// import IconBell from '../../Icons/Bell';
import HeaderUser from './HeaderUser'

export default class HeaderCabinet extends React.PureComponent {
  render() {
    return (
      <div className='header-nav-user'>
        {/* <div className='header-nav-user-item'>
          <IconBell />
        </div> */}
        <HeaderUser />
      </div>
    )
  }
}
