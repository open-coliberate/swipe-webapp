import * as React from 'react'
import IconArrow from '../../Icons/Arrow'
import defaultsrc from '../../../utils/assets/defuserpic.jpg'
import { Link } from 'react-router-dom'
import { Routes } from '../../../utils/routes'
import { Spring, animated, config } from 'react-spring/renderprops'

export default class HeaderUser extends React.PureComponent<{}, { isOpened: boolean }> {
  constructor(props: any) {
    super(props)
    this.state = {
      isOpened: false
    }
    document.addEventListener('click', this.handleClick, false)
  }

  private myRef = React.createRef<HTMLDivElement>()

  handleClick = (e: any) => {
    if (e.target === this.myRef.current || (this.myRef.current as HTMLElement).contains(e.target)) {
      this.setState({ isOpened: !this.state.isOpened })
    } else {
      this.setState({ isOpened: false })
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClick, false)
  }

  render() {
    return (
      <div className='header-nav-user-item' ref={this.myRef}>
        <img className='header-nav-user-item-userpic' src={defaultsrc} alt='userpic' />
        <div className='header-nav-user-item-arrow'>
          <IconArrow />
        </div>

        <Spring
          force
          native
          immediate={!this.state.isOpened}
          config={config.gentle}
          from={{
            opacity: this.state.isOpened ? 0 : 1,
            height: this.state.isOpened ? 0 : 'auto'
          }}
          to={{
            opacity: this.state.isOpened ? 1 : 0,
            height: this.state.isOpened ? 'auto' : 0
          }}
        >
          {props => (
            <animated.div
              className={`${
                !this.state.isOpened
                  ? 'header-nav-user-item-menu disabled'
                  : 'header-nav-user-item-menu'
              }`}
              style={props}
            >
              <Link to={Routes.Me} className='header-nav-user-item-menu-nav'>
                Account
              </Link>
              <Link to={Routes.Logout} className='header-nav-user-item-menu-nav'>
                Logout
              </Link>
            </animated.div>
          )}
        </Spring>
      </div>
    )
  }
}
