import * as React from 'react'
import { NavLink } from 'react-router-dom'

// import AnalyticsIcon from '../../../components/Icons/Analytics';
import CalendarIcon from '../../../components/Icons/Calendar'
import Setting from '../../../components/Icons/Setting'
import { Routes } from '../../../utils/routes'
import StarIcon from '../../Icons/Star'
import UserIcon from '../../Icons/User'

const serviceLinks = [
  {
    route: Routes.Calendars,
    content: {
      icon: <CalendarIcon />,
      text: 'Calendars'
    }
  },
  {
    route: Routes.Tools,
    content: {
      icon: <Setting />,
      text: 'Tools'
    }
  },
  {
    route: Routes.Admins,
    content: {
      icon: <StarIcon />,
      text: 'Super Admins'
    }
  },
  {
    route: Routes.Users,
    content: {
      icon: <UserIcon />,
      text: 'Users'
    }
  }
  // {
  //   route: Routes.Analytics,
  //   content: {
  //     icon: <AnalyticsIcon />,
  //     text: 'Analytics'
  //   }
  // }
]
const HeaderNav: React.StatelessComponent = () => {
  const links = serviceLinks.map(({ route, content }, i) => (
    <NavLink
      key={`${i}_${route}`}
      to={route}
      activeClassName='header-nav-menu-item__active'
      className='header-nav-menu-item'
    >
      {content.icon}
      {content.text}
    </NavLink>
  ))
  return <nav className='header-nav-menu'>{links}</nav>
}

export default HeaderNav
