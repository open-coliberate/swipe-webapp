import '../node_modules/normalize.css/normalize.css'
import './global-styles.css'
import 'babel-polyfill'

import { createBrowserHistory } from 'history'
import React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'

import App from './app'
import registerServiceWorker from './registerServiceWorker'
import { configureStore } from './store'

export const history = createBrowserHistory()
const store = configureStore(history)

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()
