import { createTrivialAction } from 'redux-trivial-actions'

export const createBooking = createTrivialAction('CREATE_BOOKING')
export const resetBooking = createTrivialAction('RESET_BOOKING')

export const setBookingDate = createTrivialAction('SET_BOOKING_DATE')
export const setBookingType = createTrivialAction('SET_BOOKING_TYPE')
