import { createTrivialAction } from 'redux-trivial-actions'

export const handlePopup = createTrivialAction('HANDLE_POPUP')

export const setCurrHub = createTrivialAction('SET_CURR_HUB')
