import { createTrivialAction } from 'redux-trivial-actions'

export const triggerAlert = createTrivialAction('TRIGGER_ALERT')
export const collapseAlert = createTrivialAction('COLLAPSE_ALERT')
export const fatalAlert = createTrivialAction('FATAL_ALERT')
