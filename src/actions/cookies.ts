import { createTrivialAction } from 'redux-trivial-actions'

export const setCookies = createTrivialAction('SET_COOKIES')
export const getCookies = createTrivialAction('GET_COOKIES')

export const setActivation = createTrivialAction('SET_FOR_ACTIVATION')
export const resetActivation = createTrivialAction('RESET_FOR_ACTIVATION')
